Referat 15.03:

-	Alle gruppemedlemmer var tilstede. Vi diskuterte hva som er plan for MVP videre, vi skal implementere enemies og designe ferdig spillbrett. Vi jobbet sammen for å utvikle JUnit tester for spillobjektene. Vi har og diskutert hvordan vi skal rydde opp i koden og fragmentere klassene bedre og planlagt komplett gjennomgang for å strukturere koden ved neste møte. Og vi har jobbet videre med multiplayer implementasjon og hvordan dette skal fungere grafisk på skjermen.


Referat 22.03.22:

- Alle var tilstede og vi gikk sammen gjennom koden for å planlegge sprinten før innlevering. Vi planla hva vi må implementere av tester for de forskjellige klassene, og vi har fordelt arbeidsoppgaver for å implementere tester, rydde i kode, skrive javadocs, implementere game over screen. Vi har planlagt nytt møte torsdag, og vi har gått gjennom vurdering for oblig 1 og diskutert hvordan vi skal forbedre oss.


Referat 24.03.22:

- Martin ga beskjed i forveien av møte om sykdom, han var tilgjengelig på Discord. Ellers var alle tilstede. Vi jobbet med rydding i klassene (unødvendig kode og bedre variabelnavn), samt med omstrukturering av prosjektet. Vi fordelte klassene inn i mappene Controller, model og view for bedre oversikt. Samt jobbet vi med tesktoppgavene i deloppgave 1, 2 og 3. Vi jobbet og med testene og litt med banens utseende. Vi brukte og tid på å diskutere hva som har gått bra og dårlig med denne sprinten. Samt diskuterte vi forbedringer for neste sprint. Vi fordelte og arbeidsoppgaver for gjenstående arbeid før innleveringen.

Referat 29.03.22:

- Årne, Daniel og Erik møtte, Sigurd og Martin hadde ikke mulighet/syk. Vi ryddet mer i kode, prøvd å skrive generelle draw metoder for alle objektene, har planlagt MVP for neste sprint oblig 3. Har og kommet med nye ideer for MVP/backlog.

Referat 05.04.22:

- Sigurd var bortreist og tilgjengelig på discord. Alle andre møtte. Fikk tilbakemelding fra oblig 2, og ut i fra det jobbet med punkter som måtte forbedres fra ob2. Vi fikk og hjelp med hvordan å løse test error når image i konstruktør. Vi fant design med animasjoner for Boss og eventuelle nye for karakterer. Vi har fullført spillbrett, og har planlagt arbeidsoppgaver til neste møte, og planlagt møte torsdag 7.04

Referat 07.04.22:

- Sigurd har vært bortreist denne uken, men var igjen tilgjengelig på Discord. Vi andre møttes for å plalegge siste innspurt for denne innleveringen. Vi fordelte oppgaver oss imellom med å jobbe med innskrivingsdelene av deloppgave 1-3, samt å føre inn referanser til det visuelle, og å lage ny grafdiagram for prosjektet med de nye klassene. Samt ble det brukt til på å løse noen bugs observert ved gjennomspilling. Vi fikk gjort mesteparten unnder møtet, og fordelte de resterende arbeidsoppgavene mellom oss å fullføre før innlevering.

Referat 21.04.22:

- Alle møtte utenom Årne som var bortreist. Vi rettet opp i en bug med enemy locations, vi implementerte mulighet for valg av multiplayer, og planlagte sprint for innlevering ob4. Vi fordelte arbeidsoppgaver for multiplayer implementasjon for  gjennomføring før neste møte 25.04 

Referat 25.04.22:

- Alle møtte og vi avtalte først tid for presentasjon til neste uke tirsdag. Deretter jobbet vi med implementering av boss-arena, retting opp i attack-animasjon bug, sprites for player 2 karakter, og holder på å implementere funksjon for multiplayer. Har satt nye keys for hver av karakterene og får begge printet på skjerm. Videre skal vi fortsette med funksjonene med multiplayer imorgen.

Referat 26.04.22:

- Alle møtte og vi jobbet videre med implementeringer for multiplayer og Boss. Vi fikset multiplayer implementasjoner nesten ferdig, eneste som gjenstår er revival og evnt. limits for player 2. Oppdatert playerene med hits animasjoner når de tar skade. Finpusset på menyen med nye spillinstrukser for multiplayer. Avtalt neste møte torsdag 28.04 kl 10

Referat 28.04.22:

- Erik og Daniel møtte, resten var tilgjengelige for korrespondanse og arbeidsfordeling på discord. Fikk implementer funksjon for revival av død karakter i multiplayer, dette var det gjenstående for implementering av multiplayer krav. Mellom møtene har boss krav og blitt implementert. Vi har feiltestet implementasjonene og jobbet med tekstoppgavene for innlevering. 
