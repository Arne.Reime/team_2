# Oblig 4 - *Waffle-Run*

## Team: *team_2* (Gruppe 2): *Daniel Mjøs Røli, Erik Holmeide, Martin Elias Toftevåg, Sigurd Blakkestad, Årne Reime*

Link til projectboard i [Asana](https://app.asana.com/0/1201785608693491/board)
Om den ikke fungerer er det et skjermbildet av Asana i mappen Klassediagram

### Deloppgave 1: Team og prosjekt

> Roller:
- "Vi har ikke gjort noen vesentlige endringer i roller."

> Erfaringer:
- "Vi har ved innlevering 4 bygd mye på erfaringene som var bra fra de tidligere sprintene (Scrum). Det vi gjorde desidert best denne gang og hadde veldig god erfaring med, var at hele første gruppetime ble dedikert til en grundig planlegging av hele sprinten. Vi brukte da tid på å utvikle gode brukerhistorier, arbeidskrav og arbeidsoppgaver for alle de gjenstående kravene. Vi listet først alle erbeidsoppgavene og opprettet deretter listepunktene som oppgaver i Asana (Kanban). Som vi har erfart tidligere gir dette god oversikt, og det gjorde hele denne sprinten enda mer effektiv og oversiktelig siden alt var på plass etter første gruppetime. Vi hadde enda bedre erfaring enn tdiligere med arbeidsfordeling både i gruppemøter og for arbeid mellom gruppene, mye på grunn av det grundige planleggingsarbeidet."

> Gruppedynamikk:
- "Gruppedynamikken har som vanlig funket veldig bra, hvis det har vært noe som noen har lurt på, har vi diskutert det og funnet en løsning."

> Kommunikasjon:
- "Vi har har fortsatt å være produktive i kommunikasjon over discord og i møter. Alle har er involvert i avgjørelser om prosjektet."

> Kodebase:
- "Total megde commits har noe ujevn fordeling mellom gruppemedlemmer som før, men alle har bidratt jevnt til projektet. Dette er på grunn av mye samarbeid om kodingen under gruppemøtene, samt at vi har hatt mange arbeidsoppgaver som ikke involverer koding under sprinten som f.eks. finne og tilpsse sprites, lage klassediagram, manuell testing, bug-fiksing, og tekstskriving (for planleggingen, oppdatering av arbeidsoppgaver, og for innlevering)."

> Forbedringspunkter:
- "Uttdypes under retroperspektiv for hele prosjektet."

> Retrospektiv for hele prosjektet:
- "Retroperspektiv for sprinten ob4: Som nevnt ved erfaringer har vi planlagt mye bedre for hele sprinten i begynnelsen og vi har møttes oftere. Det har bidratt til bedre arbeidsfordeling og mer effektivitet. Kommunikasjon har vær veldig god som tidligere."
- "Retroperspektiv for hele projektperioden: I løpet av projektet hav vi vært gode på å lære av og tilpasse oss etter tilbakemledingene fra gruppeleder samt forbedringer vi har sett behov for selv fra sprint til sprint. Vi falt litt fra med rollefordelingene vi tildelte i starten, så dette kunne vi satt oss mer inn i om vi skulle gjort det på nytt. Vi kunne også vært bedre på å starte å skrive statiske tester tidligere. Vi kunne og være tidligere ute med oppretting av ryddig mappestruktur og navngivning på klasser i projektet i koden, samt abstraksjon. Men alt i alt er vi fornøyd med arbeidsprosessen, at vi har tatt til oss endringer og behov effektivt."

### Referat sprint 4:

Referat 21.04.22:

- Alle møtte utenom Årne som var bortreist. Vi rettet opp i en bug med enemy locations, vi implementerte mulighet for valg av multiplayer, og planlagte sprint for innlevering ob4. Vi fordelte arbeidsoppgaver for multiplayer implementasjon for  gjennomføring før neste møte 25.04 

Referat 25.04.22:

- Alle møtte og vi avtalte først tid for presentasjon til neste uke tirsdag. Deretter jobbet vi med implementering av boss-arena, retting opp i attack-animasjon bug, sprites for player 2 karakter, og holder på å implementere funksjon for multiplayer. Har satt nye keys for hver av karakterene og får begge printet på skjerm. Videre skal vi fortsette med funksjonene med multiplayer imorgen.

Referat 26.04.22:

- Alle møtte og vi jobbet videre med implementeringer for multiplayer og Boss. Vi fikset multiplayer implementasjoner nesten ferdig, eneste som gjenstår er revival og evnt. limits for player 2. Oppdatert playerene med hits animasjoner når de tar skade. Finpusset på menyen med nye spillinstrukser for multiplayer. Avtalt neste møte torsdag 28.04 kl 10

Referat 28.04.22:

- Erik og Daniel møtte, resten var tilgjengelige for korrespondanse og arbeidsfordeling på discord. Fikk implementer funksjon for revival av død karakter i multiplayer, dette var det gjenstående for implementering av multiplayer krav. Mellom møtene har boss krav og blitt implementert. Vi har feiltestet implementasjonene og jobbet med tekstoppgavene for innlevering.

### Deloppgave 2: Krav

# Strech goal

> Fikk under sprint for oblig 4 implementert godt fungerende funksjoner for lokal multiplayer med 2 spillere. Et eventuelt videre strech goal vi kunne tenkt oss er implementere et funngerende lyddesign med lydeffekter for alle idle posisjoner, bevegelser, hopp og angrep for samtlige objekt. Samt atmosfærelyder for dør, portal, samt omgivelser. Og funnet en passende soundtrack for de forskjellige banene.  

# MVP og annet

> Vi har for sprint 4 forholdt oss til samme kravliste med samme brukerhostorier osv. som ved sprint oblig 3. Vi endret kravet om at begge spillers angrep skal ta skade når angrep samtidig, for dette gav bedre spillflyt. Vi gjorde og småendringer ved akseptansekriteriene for hvordan boss tar skade, skyter projektil, og for oppnåelse av winstate. Samt gjorde vi endringer i akseptansekriteriene og oppgavene for krav om boss-arena. Videre presenteres kravliste som brukerhistorier, alle er implementert:

- *(Imp)* Krav 1: Jeg som spiller trenger et spillbrett å se på for å ha noe visuelt å se på.
- *(Imp)* Krav 2: Jeg som spiller trenger å ha en synlig spill-karakter på brettet for å vite hva jeg skal navigere på brettet.
- *(Imp)* Krav 3: Jeg som spiller vil kunne flytte spill-karakteren til høyre og venstre for å utforske hele det tilgjengelige spillbrettet.
- *(Imp)* Krav 4: Jeg som spiller ønsker at spill-karakter kan interagere med terreng for å få et tydelig bilde av hvilke plattformer som kan benyttes for navigeringen i terrenget
- *(Imp)* Krav 5: Jeg som spiller har lyst til at spill-karakteren kan hoppe for å bytte plattformer eller hoppe på objekter.
- *(Imp)* Krav 6: Jeg som spiller ønsker å tilegne poeng i spillet som gir et mål på hvor bra jeg gjør det.
- *(Imp)* Krav 7: Jeg som spiller ønsker fiender på spillbrettet for å utfordres av mer mobile objekt og angrep.
- *(Imp)* Krav 8: Jeg som spiller ønsker at spill-karakteren kan dø/gi ‘game over’ for å motiveres til å unngå skade mot spill-karakteren.
- *(Imp)* Krav 9: Jeg som spiller ønsker et tydelig mål for spillbrettet for å motivere for progresjon i spillet. 
- *(Imp)* Krav 10: Jeg som spiller ønsker et målsymbol i slutten av spillets storyline for å få en følelse av konklusjon av spillet (*Denne er nå et vaffelsymbol*).
- (Imp) Krav 11: Jeg som spiller ønsker å kunne transporteres til nye spillbrett for å kunne engasjeres av nye utfordringer og layouts.
- *(Imp)* Krav 12: Jeg som spiller ønsker interagerbar ’game over’-skjerm for å få lettvint tilgang til start-menyens valg og nytt spill.
- *(Imp)* Krav 13: Jeg som spiller ønsker checkpoits underveis i spillet for å unngå å spille hele spillet på nytt hver gang spillkarakter faller utenfor platform.
- *(Imp)* Krav 14: Jeg som spiller ønsker å ha en ‘boss-battle’ på slutten av spillet som kan gi en utfording og engasjerende avslutningsfølelse til spillet.
- *(Imp)* Krav 15: Jeg som spiller ønsker støtte til ‘multiplayer’ for å oppleve gleden av å dele spillopplevelsen med andre folk.
- *(Imp)* Krav 16: Jeg som spiller ønsker en NPC karakterer for å få mulighet og følelsen av mer veiledning og mer liv i spillet.
- *(Imp)* Krav 17: Jeg som spiller ønsker å kunne aktivere en 'dør' for forflyttelse til ukjente steder for å få en følelse av overaskelse og spenning.
- *(Imp)* Krav 18: Jeg som spiller ønsker at spillkarakteren kan angripe fiender for å få en følelse av mer interaktivitet og styrke i spillkarakteren. 
- *(Imp)* Krav 19: Jeg som spiller ønsker å kunne finne nøkkelobjekt for å tillate tilgang til sperrede steder for å bli aktivisert av småoppdrag.
- *(Imp)* Krav 20: Jeg som spiller ønsker et visuelt og spillfunksjonelt anneledes boss-område for å få en følelse av ny utfordring, hast, og mestring tilknyttet bossen.

> Videre er kravene vi har implementert for sprint oblig 4 med akseptansekriterier og arbeidsoppgaver:

**Krav 14:** ***(Jeg som spiller ønsker å ha en ‘boss-battle’ på slutten av spillet som kan gi en utfording og engasjerende avslutningsfølelse til spillet)***  

**Akseptansekriterier**:  
    - Det skal være et synlig boss-objekt på skjermen.  
    - Boss-okbekt skal ha tyngdekraft, dimensjoner og posisjon, samt liv verdi.  
    - Boss-objektet skal bevege seg fra side til side, og hoppe.  
    - Boss-objektet skal kunne skyte skadelige prosjektiler.   
    - Spillerobjekt skal ta skade når kollisjon med boss-objekt eller prosjektil.  
    - Spillerobjekt skader boss ved angrep.  
    - Når Boss-objekt sitt liv er 0 boss objekt forsvinne.    

**Arbeidsoppgaver**:  
    - Implementere en Bossklasse med variabler for tyngdekraft, dimensjoner, liv og skade.  
    - Implementer kollisjon med platformer og spillkarakter for Boss-objekt.  
    - Implementer visuell design for all bevegelse av Boss-objekt (gange og hopp) som tilknyttes draw metode.  
    - Implementer grenser for BossObjektets bevegelse.  
    - Implementer intervall mellom Boss-objektets hopp og gange.  
    - Implementer reduksjon Boss-liv når spillkarakter treffer med angrep.  
    - Implementer reduksjon spillkarakter-liv -1 om kollisjon Boss-objekt med sidene (evnt. underside om høye hopp).  
    - Implementer metode for å skyte projektiler i horisontal retning utifra Boss-objekts retning.  
    - Implementer visuell design for prosjektil som tilknyttes draw metode.  
    - Implementer intervall for utskytelse av prosjektil.  
    - Implementer levetid for hvert projektil.  
    - Implementer kollisjon spillkarakter og prosjektil, samt reduksjon spillkarakter-liv -1 ved kollisjon.  
    - Endre gamestate til winstate når Boss-objekt liv lik 0 og spillkarakter treffer målsymbol.   
 
**Krav 15:** ***(Jeg som spiller ønsker støtte til ‘multiplayer’ for å oppleve gleden av å dele spillopplevelsen med andre folk)***  

**Akseptansekriterier**:  
    - Når multiplayer modus velges skal to spillkarakterer vises  
    - Begge spillkarakterer skal ha samme funksjon (angrep, bevegelse, livreduksjon) som spillkarakter ved enspiller-modus  
    - Når begge spillere angriper samme fiende skal førsttreffende angrep gjelde.  
    - Forskjellige utvalgte taster skal være dedikert til hver av spillkarakterene respektivt  
    - Når en av spillerene dør og en i live, skal død spiller gjenopplives om en av de har 5 vafler eller mer 

**Arbeidsoppgaver**:  
    - Få to spillkarakterer på skjermen  
    - Omstruktrere og fordele keytaster til hver av spillkaraktererene  
    - Implementere en ny gamestate for multiplayer  
    - Implementere funksjon for revival- av spillkarakterer når en er død og en i live 

**Krav 20:** ***(Jeg som spiller ønsker et visuelt og spillfunksjonelt anneledes boss-område for å få en følelse av ny <br>utfordring, hast, og mestring tilknyttet bossen)***  

**Akseptansekriterier**:  
    - Boss- området skal se visuelt annerledes ut enn banens omgivelser.  
    - Boss-området skal ha nye plasseringer av platformer.    

**Arbeidsoppgaver**:  
    - Implementer bakgrunn for arena.   
    - Plasser og implementer kollisjonsgrenser for arenaens plattformer.  

### Deloppgave 3: Produkt og kode

> Dette har vi fikset siden sist:
- Vi har fått implementert de siste kravene med klasser og funksjoner for boss, multiplayer og boss arena. For boss har vi lagd en egen bossklasse, projektilklasse med funksjonene som trengtes. For multiplayer har vi lagd eget menyvalg i TitleMeny.java, et eget state for multiplayer i Game.java, og alle vesentlige funksjoner for håndering og oppførselen til multiplayer er lagt til i GameHandler.java.
Vi har fjernet ubrukte klasser, deler av kode, filer og sprites, og prøvd å komprimere koden der det har vært unødvendige gjentagelser. 

> Testing:
- Vi har brukt spotbugs til testing av kode. Også testet med runcoverage på Eclipse. Vi har under sprinten testet mye av de nye funksjonene manuelt, og har oppdatert ManuelTesting med beskrivelser for hvordan de nye funnksjonen kan testes i spillet.
 
