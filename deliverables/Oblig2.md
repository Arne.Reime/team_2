# Oblig 2 - *Waffle-Run*

## Team: *team_2* (Gruppe 2): *Daniel Mjøs Røli, Erik Holmeide, Martin Elias Toftevåg, Sigurd Blakkestad, Årne Reime*

Link til projectboard i [Asana](https://app.asana.com/0/1201785608693491/board)

### Deloppgave 1: Team og prosjekt

- Roller 
> Rollene har fungert bra og vi har ikke gjort noen endringer på de enda.

- Andre roller
>Vi tenker å endre litt på roller ved neste møte, da ansvarsfordelingen har blitt litt annerledes enn tenkt.

- Erfaringer
>Vi har erfart at det funker veldig greit med sprinter, og kommer til å bruke det mer framover.

- Gruppedynamikk
>Gruppedynamikken har funket veldig bra, hvis det har vært noe som noen har lurt på, har vi diskutert det og funnet en løsning. 

- Kommunikasjon
>Kommunikasjonen har vært bra, på møtene er det lett å finne gode løsninger og snakke sammen. Er det noe annet har det vært lav takhøyde på å sende melding på discord, der vi har opplevd å få god hjelp så raskt noen har hatt tid.

- Retrospektiv
>Kommunikasjon har vært veldig bra på vårt team. Etter vi startet å skrive referat ble det mye mer oversiktlig for hvem som skal gjøre hva. Gruppetimene generelt har vært veldig bra, både hva som gjøres på gruppetimene, oversikt over hva vi har gjort siden sist, og planlegge neste møtet og hva som skal bli gjort til da. 

- Forbedringer
>Vi skal bli bedre på å legge til tasks i asana. At vi planlegger lengre sprinter enn til bare neste møtet.

- Kodebase
>Mesteparten av kodebasen til spillet er laget av Årne, da vi har sett det som hensiktsmessig og dele oppgavene slik at han har det. Vi så det som problematisk at alle sitter og koder på hver sin pc hjemme, og har heller tatt problemer som har oppstått I gruppetimene og gjort det meste der også på han sin pc. Vi har heller fordelt oppgaver som hvem henter grafikken til spille, testing, klassediagram og lignende til resterende gruppemeldemmer. I starten var det også mye problemer med med git, noe som gjorde at ikke alle kunne spille eller pushe så da falt det naturlig at det bare ble på en pc. Vi skal ta en ny vurdering på dette neste møte.

> Referat fra denne sprinten:

Referat 15.03:

-	Alle gruppemedlemmer var tilstede. Vi diskuterte hva som er plan for MVP videre, vi skal implementere enemies og designe ferdig spillbrett. Vi jobbet sammen for å utvikle JUnit tester for spillobjektene. Vi har og diskutert hvordan vi skal rydde opp i koden og fragmentere klassene bedre og planlagt komplett gjennomgang for å strukturere koden ved neste møte. Og vi har jobbet videre med multiplayer implementasjon og hvordan dette skal fungere grafisk på skjermen.


Referat 22.03.22:

- Alle var tilstede og vi gikk sammen gjennom koden for å planlegge sprinten før innlevering. Vi planla hva vi må implementere av tester for de forskjellige klassene, og vi har fordelt arbeidsoppgaver for å implementere tester, rydde i kode, skrive javadocs, implementere game over screen. Vi har planlagt nytt møte torsdag, og vi har gått gjennom vurdering for oblig 1 og diskutert hvordan vi skal forbedre oss.


Referat 24.03.22:

- Martin ga beskjed i forveien av møte om sykdom, han var tilgjengelig på Discord. Ellers var alle tilstede. Vi jobbet med rydding i klassene (unødvendig kode og bedre variabelnavn), samt med omstrukturering av prosjektet. Vi fordelte klassene inn i mappene Controller, model og view for bedre oversikt. Samt jobbet vi med tesktoppgavene i deloppgave 1, 2 og 3. Vi jobbet og med testene og litt med banens utseende. Vi brukte og tid på å diskutere hva som har gått bra og dårlig med denne sprinten. Samt diskuterte vi forbedringer for neste sprint. Vi fordelte og arbeidsoppgaver for gjenstående arbeid før innleveringen.

### Deloppgave 2: Krav

# Strech goal

> Som stretch goal har vi siden sprinten for innlevering oblig 1 bestemt oss for å implementere multiplayer over nettverk. Vi har prøvd å få til å etablere en kobling ved bruk av sockets, men har ikke vært vellykket med dette enda. Derfor er dette fremlagt som et implementert MVP-krav for denne sprinten. Den prioriteres med de gjenstående kravene for neste sprint. 

# MVP og annet

> Først og fremst har vi gjort noen småjusteringer i forhold til MVP'en fro oblig 1.Krav 1-5 var implementert fra første sprint, så endringene på MVK-kravene er gjort fra krav 6 og utover (her er karvene gitt som brukerhistorier):

- Krav 6: Jeg som spiller ønsker å tilegne poeng i spillet som gir et mål på hvor bra jeg gjør det.
- Krav 7: Jeg som spiller ønsker fiender på spillbrettet for å utfordres av mer mobile objekt og angrep.
- Krav 8: Jeg som spiller ønsker at spill-karakteren kan dø/gi ‘game over’ for å motiveres til å unngå skade mot spill-karakteren.
- Krav 9: Jeg som spiller ønsker et tydelig mål for spillbrettet for å motivere for progresjon i spillet. 
- Krav 10: Jeg som spiller ønsker en vaffel som målsymbol fordi det vil se kult ut og stemme med spillets tema/historie.
- Krav 11: Jeg som spiller ønsker et nytt spillbrett når det gamle er slutt for å kunne engasjeres av nye utfordringer og layouts.
- Krav 12: Jeg som spiller ønsker interagerbar ’game over’-skjerm for å få lettvint tilgang til start-menyens valg og nytt spill.
- Krav 13: Jeg som spiller ønsker checkpoits underveis i spillet for å unngå å spille hele spillet på nytt hver gang spillkarakter faller utenfor platform.
- krav 14: Jeg som spiller ønsker å ha en ‘boss-battle’ på slutten av spillet som kan gi en konkluderende avslutningsfølelse til spillet.
- Krav 15: Jeg som spiller ønsker støtte til ‘multiplayer’ for å oppleve gleden av å dele spillopplevelsen med andre folk.
- Krav 16:	Jeg som spiller ønsker flere vanskelighets grader i spillet for å alltid kunne bli utfordret.
- Krav 17:	Jeg som spiller ønsker at spillet blir vanskeligere jo lengre jeg kommer for å alltid kunne bli utfordret av spillet.

> Vi har byttet ut tidligere krav om tidsnedtelling med krav om checkpoit. Vi så det som unødvendig for utviklingen av spillet for MVP å ha med en tidsfrist, at det heller passet bedre for spillets flyt å ha med checkpoint funksjon.

> Siden sist har vi ferdig implementert krav 6-9 og 12-13, samt som krav 10, 15, 16 er påbegynte i denne sprinten.


> Videre gir vi krav fra MVP vi har prioritert for implementering denne sprinten (beskrevet med brukerhistorier, akseptansekrav, og arbeidsoppgaver).

> Implementerte krav:

- Krav 6: Poeng *(Jeg som spiller ønsker å tilegne poeng i spillet som gir et mål på hvor bra jeg gjør det)*

    - **Akseptansekriterier**: 
    - Det skal være en synlig poengscore på skjermen
    - Det skal finnes poengobjekt som spiller kan se på skjermen
    - Poengobjekt skal gi en bestemt mengde poeng
    - Når spiller beveger seg over poengobjekt skal poengscore endres og objektet fjernes
    
    - **Arbeidsoppgaver**: 
    - Implementere Waffle-klasse som poengobjekt som Player kan kollidere med, samt variabler for dimensjoner og posisjon.
    - Implementere synlig design for Wafle-objekt og draw metode for å vise objektet 
    - Implementere en poeng variabel i Player-class
    - Kode øking +1 av PLayer's poeng ved kollisjon med Waffle-objekt
    - Vis nåværende poengscore til Player på skjem
    
-	Krav 7: Fiender *(Jeg som spiller ønsker fiender på spillbrettet for å utfordres av mer mobile objekt og angrep)* 

     - **Akseptansekriterier**:
     - Det skal være et synlig synlig fiendeobjekt på skjermen
     - Fiendeobjektet skal ha tyngdekraft for å holde seg på platformer
     - Fiendeobjektet skal bevege seg hoisontalt frem og tilbake innnenfor et bestemt område    
     - Spillkarakter skal ta bestemt skade når karakter treffern fiendeobjekt fra sidene
     - Fiendeobjekt skal dø/ forsvinne om spillkarakter treffer feinde ovenifra
     
     - **Arbeidsoppgaver**:
     - Implementer Enemy-klasse som Player kan kollidere med i alle fire retninger, samt variabler for dimensjoner og start posisjon.
     - Implementer reduksjon -1 om Player kolliderer med Enemy-objektets høyre eller venstre side
     _ Implementer fjerning av fiendeobjekt om Player kolliderer med Enemy-objektets overside
     - Implementere variabler for tyngdekraft, fart og bevegelses-distanse 
     - Implementer metode for bevegelse høyre og venstre innenfor gitt distanse-variabel
     - Implementer design for fiende med nok PNG'er til å visualisere bevegelsen til Enemy-objektet
     - Implementer draw-metode for å tegne Enemy-objekt med cases for hver PNG i Enemy-objektets bevegelse
       
-	Krav 8: At spiller tar skade *(Jeg som spiller ønsker at spill-karakteren kan dø/gi ‘game over’ for å motiveres til å unngå skade mot spill-karakteren)*

    - **Akseptansekriterier**:
    - Spillkarakter skal ta skade om karakteren faller utenfor platformene
    - Spillkarakter skal ta skade om karakteren treffer et fiendeobjekt fra siden
    - Når spillkarakterens liv er 0 skal spillet avsluttes
     - **Arbeidsoppgaver**:
     -  Implementer livmengde variabel for Player
     - Implementer reduksjon -1 om PLayer faller utenfor spillkart
     - Implementer reduksjon -1 om PLayer treffer Enemy-objekt i siden (allerede arbeidsoppgave for Enemy-krav)
      Avslutt spillsyklus om Player livmengde lik 0-
     
- 	Krav 9: Målsetting *(Jeg som spiller ønsker et tydelig mål for spillbrettet for å motivere for progresjon i spillet)*
    
    - **Akseptansekriterier**: 
    - Det skal formidles en situasjonsbeskrivende og motiverende beskjed ved start av nytt spill / bane 
     
     - **Arbeidsoppgaver**
     - Lag en tekstbeskrivelse av spillets mål
     - Display denne beskrivelsen ved spillets startområde 
-	Krav 12: Game Over Screen *(Jeg som spiller ønsker interagerbar ’game over’-skjerm for å få lettvint tilgang til start-menyens valg og nytt spill)*
    
    - **Akseptansekriterier**:  
    -  Når spillkarakterens dør og spillet avsluttes skal det komme en egen game over skjermm
    - Game over skjermen skal gi mulighet for å taste inn til start meny for å kunne starte spill på nytt
     
     - **Arbeidsoppgaver**
     - Implementer en ny gamestate variabel i Game som representerer Game-Over visning
     - Lag og vis tekst forklaring tilknyttet game over gamestate som viser hvilke alternativer spiller har av key inputs for å endre gamestate
     - Implmenter key inputs for game over gamestate
      

-	Krav 13: Checkpoints *(Jeg som spiller ønsker checkpoits underveis i spillet for å unngå å spille hele spillet på nytt hver gang spillkarakter faller utenfor platform)*
    
    - **Akseptansekriterier**:
    - Det skal være et synlig checkpoint-objekt på skjermen n
    - Checkpoit lokasjonen skal være nytt punkt for respawn om spillkarakter faller utenfor plattform
     
     - **Arbeidsoppgaver**:
     - implementer ny Checkpoit-Class som Player kan kollidere med fra alle retninger
     - Implementer variabler for dimensjoner og posisjon
     - Implementer design av Waffle og draw funksjon for å vise designet på skjermen
     Implementer funksjon for å endre Player-objektets spwan variabler når Player kolliderer med Checkpoit-objekt. Ny spawn posisjon skal matche Checkpoitets posisjon 

### Deloppgave 3: Produkt og kode

- Dette har vi fikset siden sist: 
> Vi har startet med å skrive referater. Vi har fått en mer oversiktlig tekst over
> prosjektmetodikk i oblig1.md. Det er blitt lagt til link til projectboard. Vi har fikset tag
> release på siste commit. Commit meldingene er blitt mer oversiktlige og meningsfulle. 
> Aksetansekriteriene og arbeidsoppgaver er delt opp riktig og ryddet opp. Laget en kort 
> beskrivelse om prosjektet, og laget deliverables folder med filene som skal der.
> Vi har implementert tester og gjort manuelle tester. Fjernet kode og bilder som ikke 
> ble brukt.

- Klassediagram:
> Har laget klassediagram og lagt til skjermbilde som er vedlagt under klasse_diagram_waffle_run.PNG

- Tester:
> Under mappen test/java/inf112/skeleton/app har vi skrevet flere ulike 
> jUnit tester for forskjellige tester, vi har også vedlagt en tekstfil
> i mappen hvor det forklares gjennomgang av manuell testing.
