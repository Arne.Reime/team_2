# Oblig 3 - *Waffle-Run*

## Team: *team_2* (Gruppe 2): *Daniel Mjøs Røli, Erik Holmeide, Martin Elias Toftevåg, Sigurd Blakkestad, Årne Reime*

Link til projectboard i [Asana](https://app.asana.com/0/1201785608693491/board)
Om den ikke fungerer er det et skjermbildet av Asana i mappen Klassediagram

### Deloppgave 1: Team og prosjekt

> Roller:
- "Rollene har forblitt de samme som ved oblig 1. Vi bestemte oss for ikke å gjøre noen endringer med rollene da de har fungert bra."

> Erfaringer:
- "Angående team-arbeidet og prosjektmetodikken har erfaringene vært mye de samme som ved forrige innlevering. Samarbeidet blir alltid litt bedre og effektivt ettersom vi blir stadig bedre kjent med hverandres arbeidsrutiner og egenskaper."

> Gruppedynamikk:
- "Gruppedynamikken har funket veldig bra, hvis det har vært noe som noen har lurt på, har vi diskutert det og funnet en løsning."

> Kommunikasjon:
- "Vi har blitt bedre å snakke over discord og alle har blitt mer involvert i avgjørelser som blir tatt om prosjektet."

> Retrospektiv:
- Kommunikasjonen har vært bra, vi har jobbet effektivt og vært proaktiv med komunikasjon. Til neste gang kan vi planlegge litt mer utenfor sprinten.

> 3 ting vi vil forbedre til neste gang:
- "Vi ønsker mer abstraksjon i koden, og vil prøve å introdusere en abstrakt-klasse som kan inneholde feltvariebler og metoder som gjentas for flere av objektene."
- "Vi er veldig aktive når vi møter og samarbeider, men kan bli bedre på å fordele mer arbeidsoppgaver utenfor møtene."
- "Skrive mer tydeligere oppgaver på Asana slik at man enklere kan forstå hva som må gjøres. Samt være mer aktive på å markere hvilke oppgaver som er in progress og fullførte."

### Referat:

Referat 29.03.22:

- Årne, Daniel og Erik møtte, Sigurd og Martin hadde ikke mulighet/syk. Vi ryddet mer i kode, prøvd å skrive generelle draw metoder for alle objektene, har planlagt MVP for neste sprint oblig 3. Har og kommet med nye ideer for MVP/backlog.

Referat 05.04.22:

- Sigurd var bortreist og tilgjengelig på discord. Alle andre møtte. Fikk tilbakemelding fra oblig 2, og ut i fra det jobbet med punkter som måtte forbedres fra ob2. Vi fikk og hjelp med hvordan å løse test error når image i konstruktør. Vi fant design med animasjoner for Boss og eventuelle nye for karakterer. Vi har fullført spillbrett, og har planlagt arbeidsoppgaver til neste møte, og planlagt møte torsdag 7.04.

Referat 07.04.22:

- Sigurd har vært bortreist denne uken, men var igjen tilgjengelig på Discord. Vi andre møttes for å plalegge siste innspurt for denne innleveringen. Vi fordelte oppgaver oss imellom med å jobbe med innskrivingsdelene av deloppgave 1-3, samt å føre inn referanser til det visuelle, og å lage ny grafdiagram for prosjektet med de nye klassene. Samt ble det brukt til på å løse noen bugs observert ved gjennomspilling. Vi fikk gjort mesteparten unnder møtet, og fordelte de resterende arbeidsoppgavene mellom oss å fullføre før innlevering.

### Deloppgave 2: Krav

# Strech goal

> Som stretch goal har vi siden sprinten for innlevering oblig 1 bestemt oss for å implementere multiplayer over nettverk, men vi har nå ved slutten av sprinten for oblig 3 bestemt oss for å implementere lokal flerspiller først (ved neste sprint) og se om vi får tid til å gjøre det over nettverket. Vi har prøvd å få til å etablere en kobling ved bruk av sockets, men har ikke vært vellykket med dette enda. Siden portforwarding var et større problem enn tidligere antatt er målet å få det til lokalt først. 

# MVP og annet

> Vi gjort litt flere justeringer på MVP'listen denne gangen. Først og fremst har vi fjernet de tidligere krav 16 og 17 (se Oblig2.md) anngående vanskelighetsgrader, da vi ikke lenger så disse som essensielle krav for vår MVP. Videre har vi omformulert formulering av krav 11 til å handle mer om transport til ulike omgivelser istedenfor bare nye spillbrett. Ellers har vi gjort justeringer ved å legge til noen nye krav (krav 16-20) til MVP'en basert på ideer for funksjoner vi har hatt i backlog, samt funksjoner vi har betraktet som nødvendige for å hjelpe de allerede implementerte metodene med driven i spillet. Her er den oppdaterte og komplette MVP kravlisten for sprint oblig 3, gitt som brukerhistorier. Rekkefølger for bedre lesbarhet er de implementerte først, og de uimplementerte etter disse:

- *(Imp)* Krav 1: Jeg som spiller trenger et spillbrett å se på for å ha noe visuelt å se på.
- *(Imp)* Krav 2: Jeg som spiller trenger å ha en synlig spill-karakter på brettet for å vite hva jeg skal navigere på brettet.
- *(Imp)* Krav 3: Jeg som spiller vil kunne flytte spill-karakteren til høyre og venstre for å utforske hele det tilgjengelige spillbrettet.
- *(Imp)* Krav 4: Jeg som spiller ønsker at spill-karakter kan interagere med terreng for å få et tydelig bilde av hvilke plattformer som kan benyttes for navigeringen i terrenget
- *(Imp)* Krav 5: Jeg som spiller har lyst til at spill-karakteren kan hoppe for å bytte plattformer eller hoppe på objekter.
- *(Imp)* Krav 6: Jeg som spiller ønsker å tilegne poeng i spillet som gir et mål på hvor bra jeg gjør det.
- *(Imp)* Krav 7: Jeg som spiller ønsker fiender på spillbrettet for å utfordres av mer mobile objekt og angrep.
- *(Imp)* Krav 8: Jeg som spiller ønsker at spill-karakteren kan dø/gi ‘game over’ for å motiveres til å unngå skade mot spill-karakteren.
- *(Imp)* Krav 9: Jeg som spiller ønsker et tydelig mål for spillbrettet for å motivere for progresjon i spillet. 
- (Imp) Krav 11: Jeg som spiller ønsker å kunne transporteres til nye spillbrett for å kunne engasjeres av nye utfordringer og layouts.
- *(Imp)* Krav 12: Jeg som spiller ønsker interagerbar ’game over’-skjerm for å få lettvint tilgang til start-menyens valg og nytt spill.
- *(Imp)* Krav 13: Jeg som spiller ønsker checkpoits underveis i spillet for å unngå å spille hele spillet på nytt hver gang spillkarakter faller utenfor platform.
- *(Imp)* Krav 16: Jeg som spiller ønsker en NPC karakterer for å få mulighet og følelsen av mer veiledning og mer liv i spillet.
- *(Imp)* Krav 17: Jeg som spiller ønsker å kunne aktivere en 'dør' for forflyttelse til ukjente steder for å få en følelse av overaskelse og spenning.
- *(Imp)* Krav 18: Jeg som spiller ønsker at spillkarakteren kan angripe fiender for å få en følelse av mer interaktivitet og styrke i spillkarakteren. 
- *(Imp)* Krav 19: Jeg som spiller ønsker å kunne finne nøkkelobjekt for å tillate tilgang til sperrede steder for å bli aktivisert av småoppdrag.  
- Krav 10: Jeg som spiller ønsker et målsymbol i slutten av spillets storyline for å få en følelse av konklusjon av spillet (*Funksjonen for målsymbol er implementert (portalen), men visuelt symbol gjenstår*).
- Krav 14: Jeg som spiller ønsker å ha en ‘boss-battle’ på slutten av spillet som kan gi en utfording og engasjerende avslutningsfølelse til spillet.
- Krav 15: Jeg som spiller ønsker støtte til ‘multiplayer’ for å oppleve gleden av å dele spillopplevelsen med andre folk.
- Krav 20: Jeg som spiller ønsker et visuelt og spillfunksjonelt anneledes boss-område for å få en følelse av ny utfordring, hast, og mestring tilknyttet bossen.

> Videre er kravene vi har implementert og påbegynt for sprint oblig 3 med akseptansekriterier og arbeidsoppgaver. Først for de implementerte kravene:

**Krav 16:** ***(Krav 16: Jeg som spiller ønsker en NPC karakterer for å få mulighet og følelsen av mer veiledning og mer liv i spillet)***

**Akseptansekriterier**:  
    - Når spillkarakter nærmer seg posisjon til en NPC, skal en NPC karakter vises.  
    - Når spillkarakter nærmer seg posisjon til en NPC, skal det vises beskjed knapptrykk for å interagere med NPC.  
    - Når knapp trykkes for å interagere, skal beskjeden NPC vil formidle vises. 
    - Når spillkarakter inteagerer med NPC og NPC er vendt fra spillkarakterens posisjon, skal NPC vende seg mot spillkarakter.  

**Arbeidsoppgaver**:
    - Implementere en NPC klasse med feltvariabler for posisjon, dimensjoner, tyngdekraft, og retning.  
    - Implementere visuelt design av NPC og tilpass med dimensjonene.  
    - Implementer string-beskjeder som skal vises ved de forskjellige situasjonene beskrevet over.  
    - Implementer metode for print av interaksjonknapp-beskjed ved kollisjon med spillkarakter.
    - Implementer metode for visning av NPC-beskjed mens interaksjonstast trykkes og spillkarakter holder seg innenfor bestemt område rundt NPC-karakters posisjon.  
    - Implementer metode for å endre NPC-karakters retning mot spillkarakters retning når interaksjonstast trykkes.  

**Krav 17:** ***(Jeg som spiller ønsker å kunne aktivere en 'dør' for forflyttelse til ukjente steder for å få en følelse av overaskelse og spenning)***  

**Akseptansekriterier**:  
    - Når spillkarakter nærmer seg posisjonen til et dør-objekt, skal et dørobjekt vises.  
    - Når spillkarakter står ved dørobjekt, skal dørobjektet kunne interageres med ved å trykke på en bestemt tast.  
    - Om spillkarakter er tillat å bruke døren, skal spillkarakter transporteres til et område tilknyttet døren.  
    - Om spillkarakter ikke er tillat å bruke døren, skal spillkarakteren ikke transporteres.  

**Arbeidsoppgaver**:  
    - Implementere en Dør-klasse med feltvariabler for posisjon, dimensjoner, tyngdekraft.  
    - Implementer visuelt design for dør-objekt.  
    - Implementer metode for å transportere Spillkarakter ved trykk av bestemt knapp for interaksjon.  
    - Implementer metode for å transportere Spillkarakter kun om spillkarakter tilfredstiller et spesifikt krav.  

**Krav 18:** ***(Jeg som spiller ønsker at spillkarakteren kan angripe fiender for å få en følelse av mer interaktivitet og styrke i spillkarakteren)***  

**Akseptansekriterier**:  
    - Når spiller trykker på en bestemt tast, skal spillkarakteren utføre en angrepsanimasjon.  
    - Når spillerkarakter utfører angrep i nærheten av fiender, skal fienden miste liv/dø.  
    - Spillkarakter skal kun kunne utføre angrep, hvis spillkarakteren står eller går langs bakken.  
    - Spillkarakterens angrep skal ikke skade annet enn feiende-objekt.  

**Arbeidsoppgaver**:  
    - Implementere visuelle frames for animering av angreps animasjon som har bestemte dimensjoner og retning i forhold til spillkarakterens retning.  
    - Implementer funksjon for at spillkarakter skal utføre agrepsanimasjon ved tasting av bestemt tast. 
    - Implementer krav for utførelse av angrepsanimasjon utifra spillkarakterens nåværende posisjon og bevegelse ved tatsetrykk.   
    - Implementer funskjon som gir et kollisjonsområde tilknyttet angrepes-animasjonens størrelse og retning.  
    - Implementer funksjon for reduksjon av fiendeliv/død om fiendeobjekt kollisjon med angrepets kollisjonsområde.   

**Krav 19:** ***(Jeg som spiller ønsker å kunne finne nøkkelobjekt for å tillate tilgang til sperrede steder for å bli aktivisert av småoppdrag)***  

**Akseptansekriterier**:  
    - Når spillkarakter nærmer seg posisjon gitt et nøkkelobjekt, skal et nøkkelobjekt vises.  
    - Når spillkarakter beveger seg nærme nøkkelobjekt, skal beskjed om tast for interaksjon vises.  
    - Når interaskjon utført, skal beskjed om velykket interaksjon vises.    

**Arbeidsoppgaver**:  
    - Implementer Klasse for nøkkelobjekt, med feltvariabler for posisjon, dimensjoner, tyngdekraft.  
    - Implementere visuelt design av nøkkelobjektet.  
    - Implementer string-beskjed for hvilken interaksjonsknapp som må tastes ved kollisjon mellom spillkarakter og nøkkelobjekt.  
    - Implementer string-beskjed for vellyket interaksjon med nøkkelobjekt mens interaksjonstast trykkes og spillkarakter holder seg innenfor bestemt område rundt nøkkelobjektets posisjon.    
 
> Deretter for de uimplmementerte kravene (Akseptansekriterier og arbeidsoppgaver for krav 15 er ikke ferdig formulert enda)):

**Krav 14:** ***(Jeg som spiller ønsker å ha en ‘boss-battle’ på slutten av spillet som kan gi en utfording og engasjerende avslutningsfølelse til spillet)***  

**Akseptansekriterier**:  
    - Det skal være et synlig boss-objekt på skjermen.  
    - Boss-okbekt skal ha tyngdekraft, dimensjoner og posisjon, samt liv verdi.  
    - Boss-objektet skal bevege seg fra side til side, og hoppe.  
    - Boss-objektet skal kunne skyte skadelige prosjektiler.  
    - Boss-objekt skal ha flere liv.  
    - Spillerobjekt skal ta skade når kollisjon med boss-objekt eller prosjektil.  
    - Spillerobjekt skader boss ved kollisjon ovenifra.  
    - Når Boss-objekt sitt liv er 0 skal spillskjerm for at man har vunnet spillet vises.  

**Arbeidsoppgaver**:  
    - Implementere en Bossklasse med variabler for tyngdekraft, dimensjoner, liv og skade.  
    - Implementer kollisjon med platformer og spillkarakter for Boss-objekt.  
    - Implementer visuell design for for all bevegelse av Boss-objekt (gange og hopp) som tilknyttes draw metode.  
    - Implementer grenser for BossObjektets bevegelse.  
    - Implementer tidsintervall mellom Boss-objektets hopp og gange.  
    - Implementer reduksjon Boss-liv -1 om spillkarakter treffer ovenifra.  
    - Implementer reduksjon spillkarakter-liv -1 om kollisjon Boss-objekt med sidene (evnt. underside om høye hopp).  
    - Implementer metode for å skyte projektiler i horisontal retning mot spillkarakters retning utfra bosskarakters posisjon.  
    - Implementer visuell design for prosjektil som tilknyttes draw metode.  
    - Implementer fast tidsintervall for utskytelse av prosjektil.  
    - Implementer levetid for hvert projektil.  
    - Implementer kollisjon spillkarakter og prosjektil, samt reduksjon spillkarakter-liv -1 ved kollisjon.  
    - Endre gamestate til winstate når Boss-objekt liv lik 0.   
 
**Krav 15:** ***(Jeg som spiller ønsker støtte til ‘multiplayer’ for å oppleve gleden av å dele spillopplevelsen med andre folk)***  

**Akseptansekriterier**:  
    - Når multiplayer modus velges skal to spillkarakterer vises  
    - Begge spillkarakterer skal ha samme funksjon (angrep, bevegelse, livreduksjon) som spillkarakter ved enspiller-modus  
    - Når begge spillere angriper samme fiende skal begge angrep gjelde  
    - Forskjellige utvalgte taster skal være dedikert til hver av spillkarakterene respektivt  
    - Ved multiplayermodus skal ny mekanikk for poengsystemet (vaffel) gi felles liv eller lignende  
    - Når revival-modus er tilgjengelig skal spillkarakter i live gjenopplive død spillkarakter   

**Arbeidsoppgaver**:  
    - Implementer bakgrunn for arena.   
    - Implementer kollisjonsgrenser for arenaens yttergrenser (vegger, bakke og tak).  
    - Implementer platformer i boss arena som spillkarakter (og boss-objekt?) kan kollidere med (alle fire sider) og lande på.  


**Krav 20:** ***(Jeg som spiller ønsker et visuelt og spillfunksjonelt anneledes boss-område for å få en følelse av ny <br>utfordring, hast, og mestring tilknyttet bossen)***  

**Akseptansekriterier**:  
    - Boss- området skal se visuelt annerledes ut enn banens omgivelser.  
    - Boss-området skal stenge spillkarakter og boss-objekt innenfor to vegger, gulv og tak.  
    - Boss området skal ha plattformer som spillkarakter kan hoppe på for å kunne angripe boss.  

**Arbeidsoppgaver**:  
    - Implementer bakgrunn for arena.   
    - Implementer kollisjonsgrenser for arenaens yttergrenser (vegger, bakke og tak).  
    - Implementer platformer i boss arena som spillkarakter (og boss-objekt?) kan kollidere med (alle fire sider) og lande på.  

### Deloppgave 3: Produkt og kode

> Dette har vi fikset siden sist:
- Vi har blitt bedre til å legge til ting i Asana, samt fikset en måte slik gruppeleder kan se prosjektboardet. Vi har blitt bedre til å kommunisere over discord når ting i prosjektet skal bli oppdatert og sammen finne løsninger. Vi har blitt mye bedre på å gi en jevn fordeling på oppgaver slik at alle bidrar jevnt. Vi har implementert en midlertidig slutt på spillet vårt og ryddet en god del i koden. Fortsatt litt zombie kode, men blitt bedre på å fjerne kode man kanskje bare har en ide om å bruke senere, men faktisk ikke ender opp med å bruke. Laget nye hjelpe klasser som gamehandler og drawhandler for å hjelpe med å få bedre oversikt over koden og en entity klasse for bedre polymorfisme på objektene. Vi har hentet referanser for alle bildefilene i prosjektet.

> Testing:
- Vi har brukt spotbugs til testing av kode. Også testet med runcoverage på Eclipse. Til neste gang planlegger vi mer abstraksjon i koden, så det kan hende at vi kommer unna med mindre tester til neste innlevering.

