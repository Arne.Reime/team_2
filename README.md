### Waffle-Run

> *Waffle-Run* er et plattformspill basert på vaffeltorsdag der man skal navigere en spiller gjennom en bane for å rekke frem til premien i enden av løypen, en nystekt vaffel.

> For å kjøre spillet må du laste ned intellij eller eclipse. Etter det må du klone dette prosjektet til din pc, for å så åpne det i intellij eller eclipse. Du kjører appen ved å navigere til Main.java klassen i mappen src/main/java/inf112.skeleton.app og kjøre main metoden i denne klassen.

> Du tester appen ved å kjøre unit-testene i src/test/java mappen. Vi har og inkludert beskrivelse for manuell testing av spillets funksjoner i filen ManuelTesting i samme mappe.

> Bilde/animasjon/effekt referenser:
- [Main character](https://chierit.itch.io/elementals-fire-knight) 
- [Skeleton](https://astrobob.itch.io/animated-pixel-art-skeleton)
- [Purple wizard](https://luizmelo.itch.io/wizard-pack)
- [Red Wizard](https://luizmelo.itch.io/evil-wizard)
- [Portal](https://elthen.itch.io/2d-pixel-art-portal-sprites)
- [Houses](https://ansimuz.itch.io/gothicvania-town)
- [Waffle](https://www.subpng.com/png-5tfxnx/)
- [Fridge](https://reakain.itch.io/kitchen-assets)
- [Background](https://aamatniekss.itch.io/free-pixelart-tileset-cute-forest)
- [Trees](https://ansimuz.itch.io/sunnyland-expansion-pack-trees)
- [Boxes and signs](https://cainos.itch.io/pixel-art-platformer-village-props)
- [Platform](https://grimfaith.itch.io/platformer-tileset)
- [Background for boss map](https://aethrall.itch.io/demon-woods-parallax-background)
- [Boss](https://darkpixel-kronovi.itch.io/mecha-golem-free)
- [Projectile](https://stealthix.itch.io/animated-fires)