package inf112.skeleton.app.Controller;

import inf112.skeleton.app.view.Game;
import javafx.scene.input.KeyCode;

public class KeyHandler {
    Game game;

    public KeyHandler(Game game){
        this.game = game;
    }

    public void keyPressed(javafx.scene.input.KeyEvent evt) {

        KeyCode key = evt.getCode();

        if (game.singlePlayerState) {
            if (key == KeyCode.LEFT && !game.player.attack) {
                game.player.move_left = true;
            }
            if (key == KeyCode.RIGHT && !game.player.attack) {
                game.player.move_right = true;
            }
            if (key == KeyCode.Q && game.player.onGround && game.player.attackRecharge >= 50) {
                game.player.takehit = false;
                game.player.move_left = false;
                game.player.move_right = false;
                game.player.attackRecharge = 0;
                game.player.atkCounter = 0;
                game.player.attack = true;
            }
            if ((key == KeyCode.UP || key == KeyCode.SPACE) && game.player.onGround) {
                game.player.attack = false;
                game.player.jump = true;
                game.player.runCounter = 0;
            }
            if (key == KeyCode.E) {
                game.player.interact = true;
            }

            if (key == KeyCode.B && game.player.entered) {
                game.player.setCoordinates(game.player.placeholderX, game.player.placeholderY);
                game.player.entered = false;
                game.Map = 1;
                game.player.invincible = true;
            }

        }
        else {

            if (key == KeyCode.A && !game.player.attack) {
                game.player.move_left = true;
            }
            if (key == KeyCode.D && !game.player.attack) {
                game.player.move_right = true;
            }
            if (key == KeyCode.Q && game.player.onGround && game.player.attackRecharge >= 50) {
                game.player.takehit = false;
                game.player.move_left = false;
                game.player.move_right = false;
                game.player.attackRecharge = 0;
                game.player.atkCounter = 0;
                game.player.attack = true;
            }
            if ((key == KeyCode.W) && game.player.onGround) {
                game.player.attack = false;
                game.player.jump = true;
                game.player.runCounter = 0;
            }
            if (key == KeyCode.E) {
                game.player.interact = true;
            }

            if (key == KeyCode.B && game.player.entered) {
                game.player.setCoordinates(game.player.placeholderX, game.player.placeholderY);
                game.player2.setCoordinates(game.player2.placeholderX, game.player2.placeholderY);
                game.player.entered = false;
                game.player.entered = false;
                game.Map = 1;
                game.player.invincible = true;
                game.player2.invincible = true;
            }

            if (key == KeyCode.J && !game.player2.attack) {
                game.player2.move_left = true;
            }
            if (key == KeyCode.L && !game.player2.attack) {
                game.player2.move_right = true;
            }
            if (key == KeyCode.U && game.player2.onGround && game.player2.attackRecharge >= 50) {
                game.player.takehit = false;
                game.player2.move_left = false;
                game.player2.move_right = false;
                game.player2.attackRecharge = 0;
                game.player2.atkCounter = 0;
                game.player2.attack = true;
            }
            if ((key == KeyCode.I) && game.player2.onGround) {
                game.player2.attack = false;
                game.player2.jump = true;
                game.player2.runCounter = 0;
            }
        }
        
    }

    public void keyReleased(javafx.scene.input.KeyEvent evt) {
        KeyCode key = evt.getCode();

        if (game.singlePlayerState) {
            if (key == KeyCode.LEFT ) {
                game.player.move_left = false;
                game.player.runCounter = 0;
            }
            if (key == KeyCode.RIGHT) {
                game.player.move_right = false;
                game.player.runCounter = 0;
            }
            if (key == KeyCode.E) {
                game.player.interact = false;
            }
        }
        else {

            if (key == KeyCode.A) {
                game.player.move_left = false;
                game.player.runCounter = 0;
            }
            if (key == KeyCode.D) {
                game.player.move_right = false;
                game.player.runCounter = 0;
            }
            if (key == KeyCode.E) {
                game.player.interact = false;
            }
            if (key == KeyCode.J) {
                game.player2.move_left = false;
                game.player2.runCounter = 0;
            }
            if (key == KeyCode.L) {
                game.player2.move_right = false;
                game.player2.runCounter = 0;
            }
        }
    }

}
