package inf112.skeleton.app.Controller;

import inf112.skeleton.app.model.*;
import inf112.skeleton.app.view.Game;
import java.util.ArrayList;
import java.util.List;

public class GameHandler {
	Game game;
	List<Player> players;
	private final CollisionDetection cd = new CollisionDetection();

	public GameHandler(Game game, List<Player> players){
		this.game = game;
		this.players = players;
	}

	public boolean collisionOnAnySide(IGameObject obj1, IGameObject obj2) {
		return cd.checkCollision(obj1, obj2, "left") || cd.checkCollision(obj1, obj2, "right") ||
				cd.checkCollision(obj1, obj2, "up") || cd.checkCollision(obj1, obj2, "down");
	}

	public void step() {

		//PLAYER MOVING
		for (Player player : players) {
			player.canMoveLeft = true;
			player.canMoveRight = player.getX() <= 10500 || game.Map != 1;
			player.canMoveDown = true;
			player.canMoveUp = true;
		}

		//PLATFORM
		for (Platform platform : game.ListPlatforms) {
			for (Player player : players) {
				if (cd.checkCollision(player, platform, "right")) {
					player.canMoveRight = false;
				}
				if (cd.checkCollision(player, platform, "left")) {
					player.canMoveLeft = false;
				}
				if (cd.checkCollision(player, platform, "down")) {
					player.canMoveDown = false;
					player.onGround = true;
				}
				if (cd.checkCollision(player, platform, "up")) {
					player.canMoveUp = false;
					player.jump = false;
					player.frames = 0;
				}
			}
		}

		//WAFFLE
		for (Waffle waffle : new ArrayList<Waffle>(game.ListWaffle)) {
			for (Player player : players) {
				if (collisionOnAnySide(player, waffle)) {
					player.waffleScore++;
					if(game.singlePlayerState) {
						if (player.waffleScore % 5 == 0) {
							player.waffleScore -= 5;
							player.health++;
						}
					}else {
						if(players.get(0).dead && players.get(0).waffleScore >= 5) {
							players.get(0).health++;
							players.get(0).dead = false;
							players.get(0).setCoordinates(players.get(1).getX(), players.get(1).getY());
							players.get(0).waffleScore -= 5;
						}
						if(players.get(1).dead && players.get(1).waffleScore >= 5) {
							players.get(1).health++;
							players.get(1).dead = false;
							players.get(1).setCoordinates(players.get(0).getX(), players.get(0).getY());
							players.get(1).waffleScore -= 5;
						}
						if(players.get(0).dead && players.get(1).waffleScore >= 5) {
							players.get(0).health++;
							players.get(0).dead = false;
							players.get(0).setCoordinates(players.get(1).getX(), players.get(1).getY());
							players.get(1).waffleScore -= 5;
						}else if(players.get(1).dead && players.get(0).waffleScore >= 5) {
							players.get(1).health++;
							players.get(1).dead = false;
							players.get(1).setCoordinates(players.get(0).getX(), players.get(0).getY());
							players.get(0).waffleScore -= 5;
						}
					}
					game.ListWaffle.remove(waffle);
				}
			}
		}

		//ENEMY
		for (Enemy enemy : new ArrayList<Enemy>(game.ListEnemy)) {
			enemy.walk();
			for (Player player : players) {
				if (player.attack) {
					if (collisionOnAnySide(enemy, player.attackHitbox(player)) && !enemy.invincible) {
						enemy.takehitCounter = 0;
						enemy.takehit = true;
						enemy.health--;
						enemy.invincible = true;
						if (enemy.health <= 0) {
							game.ListEnemy.remove(enemy);
						}
					}
				}

				if ((cd.checkCollision(player, enemy, "down") || cd.checkCollision(player, enemy, "left") ||
						cd.checkCollision(player, enemy, "right")) && !player.invincible) {
					player.takehit = true;
					player.health--;
					player.invincible = true;
				}
			}

			if (enemy.invincible) {
				enemy.enemyInvincible();
			}

			for (Platform platform : game.ListPlatforms) {
				if (cd.checkCollision(enemy, platform, "down")) {
					enemy.canMoveDown = false;
				}
			}
		}

		//BOSS AND MAP2
		if (game.Map == 2) {
			for (Player player : game.ListPlayers) {
				if (player.waffleScore >= 1) game.setupState("win");
			}

			for (Boss boss : new ArrayList<>(game.ListBoss)) {

				for (Platform platform : game.ListPlatforms) {
					if (cd.checkCollision(boss, platform, "down")) {
						boss.canMoveDown = false;
						boss.jump = true;
						game.ListProjectile.add(new Projectile(boss));
					}
				}

				for (Player player : players) {
					if (collisionOnAnySide(player, boss) && !player.invincible) {
						player.takehit = true;
						player.health--;
						player.invincible = true;
					}

					if (boss.health <= 0) {
						game.ListBoss.remove(boss);
						game.ListWaffle.add(new Waffle(1800, 400));
						player.keyItem = true;
					}
				}
				if (boss.invincible) boss.bossInvincible();
				boss.jumpBoss();
				boss.walk();
			}

			//PROJECTILES
			for (Projectile proj : new ArrayList<>(game.ListProjectile)) {
				for (Player player : players) {
					if (collisionOnAnySide(player, proj) && !player.invincible) {
						game.ListProjectile.remove(proj);
						player.invincible = true;
						player.health--;
						player.takehit = true;
					}
				}
				if (proj.health <= 0) {
					game.ListProjectile.remove(proj);
				}
				proj.move();
			}
		}

		//SINGLEPLAYER
		if (game.singlePlayerState) {
			//NPC
			for (NPC npc : game.ListNPC) {
				npc.idle();
				npc.interactableOption = collisionOnAnySide(players.get(0), npc);

				for (Platform platform : game.ListPlatforms) {
					if (cd.checkCollision(npc, platform, "down")) {
						npc.canMoveDown = false;
					}
				}
			}

			//HOUSE
			for (House house : game.ListHouse) {
				if (house.getType() == "1") house.enter = collisionOnAnySide(players.get(0), house);

				if (house.enter && players.get(0).interact && players.get(0).onGround) {
					for (Player player : players) {
						player.previousCoordinates(player.getX(), player.getY());
						game.Map = 10;
						player.entered = true;
						player.setCoordinates(-2000, 520 - 100);
					}
				}
			}

			//PLAYER
			for(Player player : players) {

				if (player.invincible) player.playerInvincible();
				if (player.takehit) player.playerTakehit();

				player.Move();
				player.playerAttack();

				if (game.Map == 2) {
					for (Boss boss : game.ListBoss) {

						if (player.attack) {
							if (collisionOnAnySide(boss, player.attackHitbox(player)) && !boss.invincible) {
								boss.health--;
								boss.invincible = true;
							}
						}
					}
				}
			}

			//CHECKPOINT
			for (Checkpoint checkpoint : game.ListCheckpoint) {

				if (collisionOnAnySide(players.get(0), checkpoint)) {
					players.get(0).spawnX = checkpoint.getX();
					players.get(0).spawnY = checkpoint.getY() - checkpoint.getHeight() / 2.0;
					checkpoint.drawText = true;

				} else
					checkpoint.drawText = false;
			}

			//FRIDGE
			for (Fridge fridge : game.ListFridge) {
				if (collisionOnAnySide(players.get(0), fridge)) {
					fridge.drawText = true;

					if (players.get(0).interact) {
						players.get(0).keyItem = true;
					}
				} else {
					fridge.drawText = false;
				}
			}

			//PORTAL
			for (Portal portal : game.ListPortal) {
				if (collisionOnAnySide(players.get(0), portal)) {
					portal.drawText = true;

					if (players.get(0).keyItem && players.get(0).interact) {
						game.setupState("win");
					}
				} else {
					portal.drawText = false;
				}
			}

			if (players.get(0).getY() - players.get(0).getHeight() / 2.0 - 30 > Game.height) {
				players.get(0).Dead();
			}

			if (players.get(0).health <= 0) {
				players.get(0).dead = true;
				game.setupState("death");
			}

		}


		//MULTIPLAYER
		if (!game.singlePlayerState) {

			//NPC
			for (NPC npc : game.ListNPC) {
				npc.idle();
				npc.interactableOption = (collisionOnAnySide(players.get(0), npc) && !players.get(0).dead) || (collisionOnAnySide(players.get(1), npc) && !players.get(1).dead);

				for (Platform platform : game.ListPlatforms) {
					if (cd.checkCollision(npc, platform, "down")) {
						npc.canMoveDown = false;
					}
				}
			}

			//HOUSE
			for (House house : game.ListHouse) {
				if (house.getType() == "1") {
					house.enter = collisionOnAnySide(players.get(0), house) || collisionOnAnySide(players.get(1), house);
				}

				if (house.enter && (players.get(0).interact || players.get(1).interact) && (players.get(0).onGround || players.get(1).onGround)) {
					for(Player player : players) {
						player.previousCoordinates(player.getX(), player.getY());
						game.Map = 10;
						player.entered = true;
						player.setCoordinates(-2000, 520 - 100);
					}
				}
			}

			//PLAYER
			for(Player player : players) {
				if (player.invincible) player.playerInvincible();
				if (player.takehit) player.playerTakehit();

				if (player.equals(players.get(1))){
					if(players.get(0).dead) {
						player.Move();
					}else if(player.getX() - player.getWidth() < players.get(0).getX() - players.get(0).screenX || player.getX() > players.get(0).getX() + players.get(0).screenX * 2){
						if(player.getX() > players.get(0).getX()) player.canMoveRight = false;
						if(player.getX() < players.get(0).getX()) player.canMoveLeft = false;
						player.Move();
					}else {
						player.Move();
					}
				}else if(players.get(1).dead){
					player.Move();
				}else if(players.get(1).getX() - players.get(1).getWidth() < player.getX() - player.screenX || players.get(1).getX() > player.getX() + player.screenX * 2) {
					if(player.getX() > players.get(1).getX()) player.canMoveRight = false;
					if(player.getX() < players.get(1).getX()) player.canMoveLeft = false;
					player.Move();
				}else {
					player.Move();
				}

				player.playerAttack();

				if (game.Map == 2) {
					for (Boss boss : game.ListBoss) {
						if (player.attack) {
							if (collisionOnAnySide(boss, player.attackHitbox(player)) && !boss.invincible) {
								boss.health--;
								boss.invincible = true;
							}
						}
					}
				}
			}

			//CHECKPOINT
			for (Checkpoint checkpoint : game.ListCheckpoint) {
				if (collisionOnAnySide(players.get(0), checkpoint) || collisionOnAnySide(players.get(1), checkpoint)) {
					players.get(0).spawnX = checkpoint.getX();
					players.get(0).spawnY = checkpoint.getY() - checkpoint.getHeight() / 2.0;
					players.get(1).spawnX = checkpoint.getX();
					players.get(1).spawnY = checkpoint.getY() - checkpoint.getHeight() / 2.0;
					checkpoint.drawText = true;
				} else {
					checkpoint.drawText = false;
				}
			}

			//FRIDGE
			for (Fridge fridge : game.ListFridge) {
				if(collisionOnAnySide(players.get(0), fridge) || collisionOnAnySide(players.get(1), fridge)) {
					fridge.drawText = true;
					if(players.get(0).interact || players.get(1).interact){
						for(Player player : players) {
							player.keyItem = true;
						}
					}
				}
				else {
					fridge.drawText = false;
				}
			}

			//PORTAL
			for (Portal portal : game.ListPortal) {
				if(collisionOnAnySide(players.get(0), portal) || collisionOnAnySide(players.get(1), portal)) {
					portal.drawText = true;
					if(players.get(0).interact || players.get(1).interact) {
						game.setupState("win");
					}
				}else {
					portal.drawText = false;
				}
			}

			//Respawn mechanics for når én spiller eller begge dør
			if(players.get(0).getY() - players.get(0).getHeight() / 2.0 - 30 > Game.height && !players.get(1).dead) {
				if(players.get(1).onGround || players.get(1).canMoveDown && (players.get(1).getY() < 600)) {
					players.get(0).multiplayerDead(players.get(1).getX(), players.get(1).getY() - 100);
				}else {
					players.get(0).Dead();
				}
			}else if(players.get(0).getY() - players.get(0).getHeight() / 2.0 - 30 > Game.height) {
				players.get(0).Dead();
			}
			if(players.get(1).getY() - players.get(1).getHeight() / 2.0 - 30 > Game.height && !players.get(0).dead) {
				if(players.get(0).onGround || players.get(0).canMoveDown && (players.get(0).getY() < 600) ) {
					players.get(1).multiplayerDead(players.get(0).getX(), players.get(0).getY() - 100);
				}else {
					players.get(1).Dead();
				}
			}else if(players.get(1).getY() - players.get(1).getHeight() / 2.0 - 30 > Game.height) {
				players.get(1).Dead();
			}
			if(players.get(0).health <= 0) {
				players.get(0).dead = true;
			}
			if(players.get(1).health <= 0) {
				players.get(1).dead = true;
			}
			if(players.get(0).health <= 0 && players.get(1).health <= 0) {
				game.setupState("death");
			}

		}

	}
}

    