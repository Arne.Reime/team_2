package inf112.skeleton.app.Controller;

import inf112.skeleton.app.view.Game;
import inf112.skeleton.app.view.TitleMenu;
import javafx.scene.input.KeyCode;

public class KeyHandlerMenu {
    Game game;
    TitleMenu tm;

    public KeyHandlerMenu(Game game, TitleMenu tm){
        this.game = game;
        this.tm = tm;
    }

    public void keyPressed(javafx.scene.input.KeyEvent evt) {

        KeyCode key = evt.getCode();

        if (key == KeyCode.ENTER && Game.gameState == "title") {
            tm.inputEnter = true;
            game.Map = 1;
            tm.selectOption();
        }

        if (key == KeyCode.ESCAPE && (Game.gameState == "howToPlay" || Game.gameState == "about")) {
            game.setupState("title");
        }

        if (key == KeyCode.ENTER && Game.gameState == "death") {
            game.setupState("title");
        }

        if (key == KeyCode.ENTER && Game.gameState == "win" && game.Map == 1) {
            game.Map = 2;
            if(game.singlePlayerState) {
            	game.setup();
            }else {
            	game.multiplayersetup();
            }
        }

        if (key == KeyCode.ENTER && Game.gameState == "win" && game.Map == 2) {
            game.setupState("title");
        }

        if (key == KeyCode.DOWN && !tm.inputDown) {
            tm.inputDown = true;
            tm.update(1);
        }
        if (key == KeyCode.UP && !tm.inputUp) {
            tm.inputUp = true;
            tm.update(-1);
        }
    }

        public void keyReleased(javafx.scene.input.KeyEvent evt) {

            KeyCode key = evt.getCode();

            if (key == KeyCode.DOWN) {
                tm.inputDown = false;
            }
            if (key == KeyCode.UP) {
                tm.inputUp = false;
            }
            if (key == KeyCode.ENTER) {
                tm.inputEnter = false;
            }
        }

}
