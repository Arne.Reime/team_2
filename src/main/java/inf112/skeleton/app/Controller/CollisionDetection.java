package inf112.skeleton.app.Controller;

import inf112.skeleton.app.model.IGameObject;

public class CollisionDetection {

    public boolean checkCollision(IGameObject gameobject1, IGameObject gameobject2, String line) {
        // Gameobject1 sides
        double p_LeftSide = gameobject1.getX() - gameobject1.getWidth() / 2;
        double p_RightSide = gameobject1.getX() + gameobject1.getWidth() / 2;
        double p_DownSide = gameobject1.getY() + gameobject1.getHeight() / 2;
        double p_UpperSide = gameobject1.getY() - gameobject1.getHeight() / 2;

        // Gameobject2 sides
        double w_LeftSide = gameobject2.getX() - gameobject2.getWidth() / 2;
        double w_RightSide = gameobject2.getX() + gameobject2.getWidth() / 2;
        double w_DownSide = gameobject2.getY() + gameobject2.getHeight() / 2;
        double w_UpperSide = gameobject2.getY() - gameobject2.getHeight() / 2;

        boolean leftsideCollide = p_LeftSide - gameobject1.getSpeed() < w_RightSide && p_LeftSide > w_LeftSide;
        boolean rightsideCollide = p_RightSide < w_RightSide && p_RightSide + gameobject1.getSpeed() > w_LeftSide;
        boolean heightCollide = p_DownSide > w_UpperSide && p_UpperSide < w_DownSide;

        boolean downsideCollide = p_DownSide + gameobject1.getGravity() > w_UpperSide && p_DownSide < w_DownSide;
        boolean upsideCollide = p_UpperSide > w_UpperSide && p_UpperSide - 6 < w_DownSide;
        boolean sidesCollide = p_LeftSide < w_RightSide && p_RightSide > w_LeftSide;

        // CHECK LEFT
        if (leftsideCollide && heightCollide && line.equals("left")) {
            return true;
        }

        // CHECK RIGHT
        if (rightsideCollide && heightCollide && line.equals("right")) {
            return true;
        }

        //CHECK DOWN
        if (downsideCollide && sidesCollide && line.equals("down")) {
            return true;
        }

        //CHECK UP
        if (upsideCollide && sidesCollide && line.equals("up")) {
            return true;
        }

        return false;
    }
}
