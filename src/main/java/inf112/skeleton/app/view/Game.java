package inf112.skeleton.app.view;

import inf112.skeleton.app.Controller.GameHandler;
import inf112.skeleton.app.model.*;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import inf112.skeleton.app.Controller.KeyHandler;
import inf112.skeleton.app.Controller.KeyHandlerMenu;
import inf112.skeleton.app.model.Platform;

public class Game extends Application {
    private GameHandler gamehandler;
    private AnimationTimer timer;
    private Canvas canvas;
    private long nanosPerStep = 1000_000_000L / 60L;
    private long timeBudget = nanosPerStep;
    private long lastUpdateTime = 0L;
    private Scene scene;
    public boolean singlePlayerState;

    public static final double height = 700;
    public static final double width = 1100;
    double moveSky = 0.1;

    //ENTITY LIST
    public List<Platform> ListPlatforms = new ArrayList<>();
    public List<Waffle> ListWaffle = new ArrayList<>();
    public List<Enemy> ListEnemy = new ArrayList<>();
    public List<Tree> ListTrees = new ArrayList<>();
    public List<Checkpoint> ListCheckpoint = new ArrayList<>();
    public List<NPC> ListNPC = new ArrayList<>();
    public List<House> ListHouse = new ArrayList<>();
    public List<Fridge> ListFridge = new ArrayList<>();
    public List<Portal> ListPortal = new ArrayList<>();
    public List<Player> ListPlayers = new ArrayList<>();
    public List<Projectile> ListProjectile = new ArrayList<>();
    public List<Boss> ListBoss = new ArrayList<>();

    public Player player;
    public Player player2;

    //UI IMAGES
    Image hearts = new Image("file:src/Images/Objects/heart.png");
    Image waffleImg = new Image("file:src/Images/Objects/waffle.png");

    //BACKGORUND IMAGES
    Image background1_Map1 = new Image("file:src/Images/Background/BG1.png");
    Image background2_Map1 = new Image("file:src/Images/Background/BG2.png");
    Image background3_Map1 = new Image("file:src/Images/Background/BG3.png");
    Image background_Map2 = new Image("file:src/Images/Background/parallax-demon-woods-bg.png");
    Image closeTrees_Map2 = new Image("file:src/Images/Background/parallax-demon-woods-close-trees.png");
    Image midTrees_Map2 = new Image("file:src/Images/Background/parallax-demon-woods-mid-trees.png");
    Image farTrees_Map2 = new Image("file:src/Images/Background/parallax-demon-woods-far-trees.png");

    private final TitleMenu tm = new TitleMenu(this);

    //GAME STATES
    public static String gameState;
    private final String titleState = "title";
    private final String playState = "play";
    private final String deathState = "death";
    private final String howToPlayState = "howToPlay";
    private final String winState = "win";
    private final String aboutState = "about";

    public int Map;

    public static void startIt(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        double width = Game.width;
        double height = Game.height;

        StackPane root = new StackPane();
        this.scene = new Scene(root, width, height);
        stage.setScene(scene);

        canvas = new Canvas(width, height);
        canvas.widthProperty().bind(scene.widthProperty());
        canvas.heightProperty().bind(scene.heightProperty());

        setupState("title");

        timer = new AnimationTimer() {

            @Override
            public void handle(long now) {
                if (lastUpdateTime > 0) {
                    timeBudget = Math.min(timeBudget + (now - lastUpdateTime), 10 * nanosPerStep);
                }
                lastUpdateTime = now;

                while (timeBudget >= nanosPerStep) {
                    timeBudget = timeBudget - nanosPerStep;
                    step();
                }
                draw();
            }

        };
        root.getChildren().add(canvas);

        timer.start();
        stage.show();
    }

    public void setupState(String state) {
        switch (state) {
            case titleState -> gameState = titleState;
            case deathState -> gameState = deathState;
            case howToPlayState -> gameState = howToPlayState;
            case winState -> gameState = winState;
            case aboutState -> gameState = aboutState;
        }
        KeyHandlerMenu keyS = new KeyHandlerMenu(this, tm);
        scene.setOnKeyPressed(keyS::keyPressed);
        scene.setOnKeyReleased(keyS::keyReleased);
    }

    public void setup() {
        singlePlayerState = true;
        gameState = playState;
        ListEnemy.clear();
        ListPlatforms.clear();
        ListWaffle.clear();
        ListTrees.clear();
        ListCheckpoint.clear();
        ListNPC.clear();
        ListHouse.clear();
        ListFridge.clear();
        ListPortal.clear();
        ListPlayers.clear();
        ListProjectile.clear();
        ListBoss.clear();

        player = new Player();
        ListPlayers.add(player);
        this.gamehandler = new GameHandler(this, ListPlayers);

        KeyHandler keyH = new KeyHandler(this);
        scene.setOnKeyPressed(keyH::keyPressed);
        scene.setOnKeyReleased(keyH::keyReleased);

        if (Map == 1) {
            Map1.map1Setup(this);
        }
        if (Map == 2) {
            player.keyItem = false;
            Map2.map2Setup(this);
        }
    }

    protected void step() {
        if (gameState == playState) {
            gamehandler.step();
        }
    }

    protected void draw() {
        GraphicsContext context = canvas.getGraphicsContext2D();
        context.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());

        context.save();
        context.setTextAlign(TextAlignment.CENTER);
        Color c = Color.rgb(245, 187, 131);
        context.setFill(c);
        context.fillRect(0, 0, this.width, this.height);
        context.setFill(Color.BLACK);

        double x = this.width / 2;
        double y = this.height / 3;

        if (gameState == playState) {
            context.restore();
            DrawHandler.draw(this, context);
        }

        if (gameState == titleState) {
            tm.drawMenu(context, x, y);
            context.restore();
        }
        if (gameState == deathState) {
            context.setFont(new Font("Courier", 60));
            context.fillText("Game Over", x, y);
            context.setFont(new Font("Courier", 30));
            context.fillText("You died", x, y + 70);
            context.fillText("Press enter to continue", x, y + 120);
            context.restore();
        }
        if (gameState == howToPlayState) {
            context.setFont(new Font("Courier", 25));
            context.fillText("""
                    Reach the end of the map and
                    acquire the final waffle to win.
                    Collide with wooden posts to
                    update your respawn location.
                    
                    For SinglePlayer use arrow keys to move left and right.
                    Press SPACE or up arrow to jump.
                    Interact with objects and doors
                    by pressing E.
                    Attack enemies by pressing Q.
                    Exit buildings by pressing B.
                    
                    For MultiPlayer player1 use WASD for moving
                    and jumping, Q for attack and E to interact with
                    objects.
                    Player2 use IJKL for moving and jumping,
                    U for attack E to interact with objects
                    and B to exit buildings. """, x, y-140);
            context.setFill(Color.RED);
            context.fillText("Press esc to go back", x, y + 430);
            context.restore();
        }
        if (gameState == aboutState) {
            context.setFont(new Font("Courier", 30));
            context.fillText("""
                    Waffle-Run is a platform game
                    based on Waffle Thursday at
                    Høyteknologisenteret, where
                    you have to navigate a player
                    through a course to reach the
                    prize at the end, a freshly
                    baked waffle.""", x, y-70);
            context.setFill(Color.RED);
            context.fillText("Press esc to go back", x, y + 350);
            context.restore();
        }
        if (gameState == winState && Map == 1) {
            context.setFont(new Font("Courier", 60));
            context.fillText("Level 1 complete!", x, y);
            context.setFont(new Font("Courier", 30));
            context.fillText("Press enter to continue", x, y + 80);
            context.restore();
        }
        if (gameState == winState && Map == 2) {
            context.setFont(new Font("Courier", 50));
            context.fillText("""
                    You Win!
                    You got the final waffle, enjoy!
                    """, x, y);
            context.setFont(new Font("Courier", 30));
            context.fillText("Press enter to continue", x, y + 200);
            context.restore();
        }
    }

	public void multiplayersetup() {
        singlePlayerState = false;
		// TODO Auto-generated method stub
        gameState = playState;
        ListEnemy.clear();
        ListPlatforms.clear();
        ListWaffle.clear();
        ListTrees.clear();
        ListCheckpoint.clear();
        ListNPC.clear();
        ListHouse.clear();
        ListFridge.clear();
        ListPortal.clear();
        ListPlayers.clear();
        ListProjectile.clear();
        ListBoss.clear();
        
		this.player = new Player();
		this.player2 = new Player();
		this.player2.GetPlayer2Image();
        ListPlayers.add(player);
        ListPlayers.add(player2);
		this.gamehandler = new GameHandler(this, ListPlayers);
		
		
//		multiplayer mp1 = new multiplayer(this, this.player);
//		multiplayer mp2 = new multiplayer(this, this.player2);
//		
//		
//		
//		Thread t1 = new Thread();
//		Thread t2 = new Thread();
//		t1.start();
//		t2.start();
		
        KeyHandler keyH = new KeyHandler(this);
        scene.setOnKeyPressed(keyH::keyPressed);
        scene.setOnKeyReleased(keyH::keyReleased);

        if (Map == 1) {
            Map1.map1Setup(this);
        }
        if (Map == 2) {
            Map2.map2Setup(this);
        }
	}
}
