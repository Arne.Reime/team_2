package inf112.skeleton.app.view;

import inf112.skeleton.app.model.*;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class DrawHandler {

    private static void drawObject(GraphicsContext gc, IGameObject obj, double offset) {
        double obj_w = obj.getDrawWidth() * obj.getDirection();
        double obj_h = obj.getDrawHeight();


        double obj_xPos = (obj.getX() - obj_w / 2.0) - offset;
        double obj_yPos = (obj.getY() - obj_h / 2.0);
        if (obj instanceof Player) {
            obj_yPos = (obj.getY() - obj_h / 2.0) - 60;
        }

        gc.drawImage(obj.getSprite(), obj_xPos, obj_yPos, obj_w, obj_h);
    }
    

    private static void drawText(GraphicsContext gc, int width, int height, String text) {
        double text_xPos = Game.width / 2 - width / 2.0;
        double text_yPos = Game.height / 3.5 - height / 2.0;

        gc.setFill(Color.rgb(0, 0, 0, 0.8));
        gc.fillRoundRect(text_xPos, text_yPos, width, height, 35, 35);

        gc.setFill(Color.WHITE);
        gc.setFont(new Font("Comic sans MS", 20));
        gc.fillText(text, text_xPos + 20, text_yPos + 50);

        gc.setStroke(Color.WHITE);
        gc.setLineWidth(5);
        gc.strokeRoundRect(text_xPos, text_yPos, width, height, 35, 35);
        gc.restore();
    }

    public static void draw(Game game, GraphicsContext context){
        double offset;
        if(!game.player.dead) {
        	offset =  game.player.getX() - game.player.screenX;
        }else {
        	offset =  game.player2.getX() - game.player2.screenX;
        }
        
        //MAP 1
        if (game.Map == 1) {
            //BACKGROUND
            context.drawImage(game.background1_Map1, 0, 0, Game.width, Game.height);
            int bg2Index = (int) (-game.moveSky / Game.width) - 1;

            for (int i = 0; i < 3; i++)
                context.drawImage(game.background2_Map1, Game.width * (bg2Index + i) - game.moveSky, 0, Game.width, Game.height);
            game.moveSky += 0.1;

            int bg3Index = (int) (game.player.getX() * 0.15 / Game.width) - 1;
            for (int i = 0; i < 3; i++)
                context.drawImage(game.background3_Map1, Game.width / 2 + Game.width * (bg3Index + i) - game.player.getX() * 0.15 + game.player.screenX, 0, Game.width, Game.height);
        }

        if (game.Map == 2) {
            context.drawImage(game.background_Map2, 0, 0, Game.width, Game.height);

            int bgtreesIndex = (int) (game.player.getX() * 0.15 / Game.width) - 1;

            for (int i = 0; i < 3; i++)
                context.drawImage(game.farTrees_Map2, Game.width / 2 + Game.width * (bgtreesIndex + i) - game.player.getX() * 0.15 + game.player.screenX, 0, Game.width, Game.height);

            for (int i = 0; i < 3; i++)
                context.drawImage(game.midTrees_Map2, Game.width / 2 + Game.width * (bgtreesIndex + i) - game.player.getX() * 0.20 + game.player.screenX, 0, Game.width, Game.height);

            for (int i = 0; i < 3; i++)
                context.drawImage(game.closeTrees_Map2, Game.width / 2 + Game.width * (bgtreesIndex + i) - game.player.getX() * 0.25 + game.player.screenX, 0, Game.width, Game.height);

            if (game.player.keyItem) {
                drawText(context, 250, 110, "Collect the last waffle\n" +
                                                                "at the end of the map");
            }

            //BOSS
            for (Boss boss : game.ListBoss) {
                drawObject(context, boss, offset);

                //NAME TAG
                context.setFill(Color.WHITE);
                context.setFont(new Font("Courier", 20));
                context.fillText("Anya", (boss.getX() - boss.getWidth() / 2.0) - offset + 5, boss.getY() - (boss.getHeight() / 2.0) - 22);
                context.restore();

                //BOSS HEALTH BAR
                context.setFill(Color.BLACK);
                context.fillRect((boss.getX() - boss.getWidth() / 2.0) - offset - 2, boss.getY() - (boss.getHeight() / 2.0) - 16, 67, 14);
                context.setFill(Color.RED);
                context.fillRect((boss.getX() - boss.getWidth() / 2.0) - offset, boss.getY() - (boss.getHeight() / 2.0) - 14, 9 * boss.health, 10);
                context.restore();
            }
        }

        //MAP 10 (ROOM IN HOUSE)
        if (game.Map == 10) {
            context.setFill(Color.BLACK);
            context.fillRect(0, 0, Game.width, Game.height);
        }

        //HOUSE
        for (House house : game.ListHouse) {
            if (house.getType() == "1") drawObject(context, house, offset - 25);
            if (house.getType() == "2") drawObject(context, house, offset);

            //hitbox
            //context.fillRect((house.getX() - house.getWidth()/2.0) - game.player.getX() + game.player.screenX, house.getY() - house.getHeight()/2.0, house.getWidth() + 20, house.getHeight());
        }

        //TREE
        for (Tree tree : game.ListTrees) {
            drawObject(context, tree, offset);
        }

        //WALLS
        for (Platform platform : game.ListPlatforms) {
            drawObject(context, platform, offset);
        }


        //ENEMY
        for (Enemy enemy : game.ListEnemy) {
            //hitbox
            //context.fillRect((enemy.getX() - enemy.getWidth()/2.0) - game.player.getX() + game.player.screenX, enemy.getY() - enemy.getHeight()/2.0, enemy.getWidth(), enemy.getHeight());
            drawObject(context, enemy, offset + (10 * enemy.getDirection()));

            //ENEMY HEALTH BAR
            context.setFill(Color.BLACK);
            context.fillRect((enemy.getX() - enemy.getWidth() / 2.0) - offset - 2, enemy.getY() - 58,64,14);
            context.setFill(Color.RED);
            context.fillRect((enemy.getX() - enemy.getWidth() / 2.0) - offset, enemy.getY() - 56, 20 * enemy.health,10);
            context.restore();
        }

        //NPC
        for (NPC npc : game.ListNPC) {
            drawObject(context, npc, offset);

            if (npc.interactableOption && !game.player.interact) {
                String talkToNPCOption = "Hold E to interact with npc";
                drawText(context, 300, 80, talkToNPCOption);
            }
            if (npc.interactableOption && game.player.interact) {
                npc.changeDirection(game);

                if (npc.getType() == "BlueWizard") {
                    npc.NPCDialogue = """
                            Who goes there! Oh greetings,
                             young traveler. I have heard
                             a rumor that there is a mighty fine
                            treasure for the one who dear cross
                             the forbidden lands.""";
                    drawText(context, 400, 200, npc.NPCDialogue);
                }

                if (npc.getType() == "RedWizard") {
                    if (game.player.keyItem) {
                        npc.NPCDialogue = """
                                Congratulations! You're a
                                worthy hero deserving to continue your
                                grand quest. Please be so kind to step
                                into the portal now.
                                Good luck with your journey.""";
                    }
                    else {
                        npc.NPCDialogue = """
                                If you want to continue
                                your journey into the portal,
                                i suggest you grab the homemade jam
                                from the tallest house in this
                                town. Then you should be worthy.""";
                    }
                    drawText(context, 400, 200, npc.NPCDialogue);
                }
            }
        }

        //WAFFLE
        for (Waffle waffle : game.ListWaffle) {
            drawObject(context, waffle, offset);
        }

        //CHECKPOINTS
        for (Checkpoint checkpoint : game.ListCheckpoint) {
            drawObject(context, checkpoint, offset);
            if (checkpoint.drawText) {
                String checkpointText = "Checkpoint updated";
                drawText(context, 240, 80, checkpointText );
            }
        }

        //FRIDGE
        for (Fridge fridge : game.ListFridge) {
            drawObject(context, fridge, offset);
            if (fridge.drawText && !game.player.interact) {
                String jamText = "Hold E to retrieve jam from fridge";
                drawText(context, 360, 90, jamText);
            }
            if (fridge.drawText && game.player.interact) {
                String jamRetrieved = """
                        You have retrieved the
                        jam. Time to head back.
                         Press B to leave the house.""";
                drawText(context, 300, 150, jamRetrieved);
            }
        }

        //hitbox player
        //context.fillRect(game.player.screenX - game.player.getWidth()/2.0, game.player.getY() - game.player.getHeight()/2.0, game.player.getWidth(), game.player.getHeight());


        //hitbox player attack
        //if (game.player.attack) {
        //    context.fillRect((game.atk.getX() - game.atk.getWidth()/2.0) - offset, game.atk.getY() - game.atk.getHeight()/2.0, game.atk.getWidth(), game.atk.getHeight());
        //}


        //PORTAL
        for (Portal portal : game.ListPortal) {
            //hitbox
            //context.fillRect((portal.getX() - portal.getWidth()/2.0) - game.player.getX() + game.player.screenX, portal.getY() - portal.getHeight()/2.0, portal.getWidth(), portal.getHeight());
            if (portal.drawText && game.player.keyItem) {
                String portalMessage = """
                        Press E to progress your journey""";
                drawText(context, 340, 100, portalMessage);
            }
            drawObject(context, portal, offset + 30);
        }

        //PLAYER
        if(!game.player.dead) {
        	drawObject(context, game.player, game.player.getX() - game.player.screenX + 5 * game.player.getDirection());
        }
        if(!game.singlePlayerState) {
        	if(!game.player2.dead) {
        		drawObject(context, game.player2, offset);
        	}
        }

        //PROJECTILES
        if (game.Map == 2) {
            context.setFill(Color.RED);
            for (Projectile proj : game.ListProjectile) {
                drawObject(context, proj, offset - 35*proj.getDirection());

                //hitbox
                //context.fillRect((proj.getX() - proj.getWidth()/2.0) - offset, proj.getY() - proj.getHeight()/2.0, proj.getWidth(), proj.getHeight());
            }
        }

        //START TEXT
        if (game.player.getX() < 200 && game.Map == 1 && !game.player.dead) {
            String startText = """
                    Hi! Waffle Thursday is closed and due
                     to an error in the system, the very
                     last waffle is made for two people,
                     you and Anya!!! Battle against Anya
                     to secure the last waffle.""";
            drawText(context, 400, 200, startText);
        }else if(!game.singlePlayerState) {
        	if (game.player2.getX() < 200 && game.Map == 1 && !game.player2.dead) {
                String startText = """
                        Hi! Waffle Thursday is closed and due
                         to an error in the system, the very
                         last waffle is made for two people,
                         you and Anya!!! Battle against Anya
                         to secure the last waffle.""";
                drawText(context, 400, 200, startText);
            }
        }
        //HEARTS
        for (int i = 1; i <= game.player.health; i++) {
            int w = 45;
            int h = 45;
            double xhearts = 50 * i - w / 2.0;
            double yhearts = 50 - h / 2.0;

            context.drawImage(game.hearts, xhearts - w / 2.0, yhearts - h / 2.0, w, h);
        }
        
        if(!game.singlePlayerState) {
        	for (int i = 1; i <= game.player2.health; i++) {
                int w = 45;
                int h = 45;
                double xhearts = 1100- (50 * i - w / 2.0);
                double yhearts = 50 - h / 2.0;

                context.drawImage(game.hearts, xhearts - w / 2.0, yhearts - h / 2.0, w, h);
            }
        }

        //WAFFLESCORE DISPLAY
        context.drawImage(game.waffleImg, 10, 55, 50, 50);
        context.setFill(Color.WHITE);
        context.setFont(Font.font("Verdana", FontWeight.BOLD, 35));
        context.fillText("x" + game.player.waffleScore, 65, 95);
        context.restore();

        if(!game.singlePlayerState) {
        	context.drawImage(game.waffleImg, 1100-65-50-10, 55, 50, 50);
            context.setFill(Color.WHITE);
            context.setFont(Font.font("Verdana", FontWeight.BOLD, 35));
            context.fillText("x" + game.player2.waffleScore, 1100-65, 95);
            context.restore();
        }
        
        //ATTACK RECHARGE
        context.setFill(Color.BLACK);
        context.fillRect(8, 118,154,24);
        if (game.player.attackRecharge >= 50) {
            context.setFill(Color.BLUE);
        }
        else {
            context.setFill(Color.LIGHTBLUE);
        }
        context.fillRect(10, 120,30 * (game.player.attackRecharge/10),20);
        context.restore();
        
        if(!game.singlePlayerState) {
        	context.setFill(Color.BLACK);
            context.fillRect(1100-154-8, 118,154,24);
            if (game.player2.attackRecharge >= 50) {
                context.setFill(Color.BLUE);
            }
            else {
                context.setFill(Color.LIGHTBLUE);
            }
            context.fillRect(1100-154-6, 120,30 * (game.player2.attackRecharge/10),20);
            context.restore();
        }
    }


}
