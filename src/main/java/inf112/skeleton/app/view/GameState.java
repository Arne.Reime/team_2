package inf112.skeleton.app.view;

public enum GameState {

	TITLE,
	PLAY,
	DEATH,
	HOWTOPLAY,
	WIN;
}
