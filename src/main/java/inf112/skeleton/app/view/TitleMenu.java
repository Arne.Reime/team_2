package inf112.skeleton.app.view;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class TitleMenu {
    Game game;
    public boolean inputDown = false;
    public boolean inputUp = false;
    public boolean inputEnter = false;
    private int selected = 0;

    public TitleMenu(Game game){
        this.game = game;
    }

    private final String[] options = {"SinglePlayer", "MultiPlayer", "How to play", "About"};

    public void update(int num) {
        selected += num;

        int len = options.length;
        if (selected < 0) selected += len;
        if (selected >= len) selected -= len;
    }

    public void selectOption() {
        switch (selected) {
            case 0 -> game.setup();
            case 1 -> game.multiplayersetup();
            case 2 -> game.setupState("howToPlay");
            case 3 -> game.setupState("about");
        }
    }

    public void drawMenu(GraphicsContext gc, double x, double y) {
        gc.save();
        gc.setTextAlign(TextAlignment.CENTER);
        gc.setFont(new Font("Comic sans MS", 60));
        gc.fillText("Waffle Run", x, y);

        for (int i = 0; i < options.length; i++) {
            gc.setFont(new Font("Courier", 40));
            String msg = options[i];
            gc.setFill(Color.rgb(0, 0, 0, 0.7));

            if (i == selected) {
                gc.setFill(Color.rgb(255, 255, 255));
                msg = "> " + msg + " <";
            }
            gc.fillText(msg, x, y + 80 + (i*55));
        }
        gc.restore();
    }
}
