package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class House extends Entity implements IGameObject {
    public boolean enter;

    public House(int X, int Y, String type) {
        this.x = X;
        this.y = Y;
        this.type = type;
        if (type == "1") {
            this.w = 50;
            this.drawW = 300;
            this.h = 400;
            this.img = new Image("file:src/Images/Objects/House1.png");
        }

        if (type == "2") {
            this.w = 40;
            this.drawW = 370;
            this.h = 290;
            this.img = new Image("file:src/Images/Objects/House2.png");
        }
    }
    @Override
    public int getDrawWidth() {
        return this.drawW;
    }
}
