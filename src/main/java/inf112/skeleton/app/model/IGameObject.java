package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public interface IGameObject {

    Image getSprite();

    int getDirection();

    int getHeight();

    int getWidth();

    int getDrawWidth();

    int getDrawHeight();

    double getX();

    double getY();

    double getSpeed();

    int getGravity();
}
