package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Portal extends Entity implements IGameObject {
    private Image p1, p2 , p3, p4, p5, p6, p7, p8;
    private int runCounter = 0;

    public boolean drawText;

    public Portal(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 50;
        this.h = 130;
        this.drawW = 100;
        GetPortalImage();
        this.direction = -1;
    }

    @Override
    public int getDrawWidth() {
        return this.drawW;
    }

    private void GetPortalImage() {
        this.p1 = new Image("file:src/Images/Portal/Portal1.png");
        this.p2 = new Image("file:src/Images/Portal/Portal2.png");
        this.p3 = new Image("file:src/Images/Portal/Portal3.png");
        this.p4 = new Image("file:src/Images/Portal/Portal4.png");
        this.p5 = new Image("file:src/Images/Portal/Portal5.png");
        this.p6 = new Image("file:src/Images/Portal/Portal6.png");
        this.p7 = new Image("file:src/Images/Portal/Portal7.png");
        this.p8 = new Image("file:src/Images/Portal/Portal8.png");
    }

    @Override
    public Image getSprite() {

        if (runCounter > 56) {
            runCounter = 0;
        }

        switch (runCounter) {
            case 0 -> this.img = p1;
            case 7 -> this.img = p2;
            case 14 -> this.img = p3;
            case 21 -> this.img = p4;
            case 28 -> this.img = p5;
            case 35 -> this.img = p6;
            case 42 -> this.img = p7;
            case 49 -> this.img = p8;
        }

        runCounter++;

        return this.img;

    }
}


