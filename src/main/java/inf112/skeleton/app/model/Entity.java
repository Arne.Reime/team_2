package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Entity {

    //ENTITY VALUES
    double x, y;
    int w, h;
    int drawW;
    double speed = 0;
    int gravity = 5;
    int direction = 1;
    Image image;

    //ONLY FOR NON-MOVING OBJECTS
    Image img;
    String type;

    public String getType() {
        return this.type;
    }

    public Image getSprite(){
        return this.img;
    }

    public int getHeight() {
        return this.h;
    }

    public int getWidth() {
        return this.w;
    }

    public int getDrawWidth() {
        return this.w;
    }

    public int getDrawHeight() {
        return this.h;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public double getSpeed() {
        return this.speed;
    }

    public int getGravity() {
        return this.gravity;
    }

    public int getDirection() {
        return this.direction;
    }
}
