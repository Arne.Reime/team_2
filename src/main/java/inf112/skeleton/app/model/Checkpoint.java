package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Checkpoint extends Entity implements IGameObject {
    public boolean drawText;

    public Checkpoint(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 40;
        this.h = 60;
        this.img = new Image("file:src/Images/Objects/checkpoint.png");
    }
    
    /**
     * 	Use ONLY for testing!
     *  Test does not run if we create a new instance of Image in the constructor.
     * @param X Coordinate
     * @param Y Coordinate
     * @param test is not used
     */
    public Checkpoint(int X, int Y, int test) {
        this.x = X;
        this.y = Y;
        this.w = 40;
        this.h = 60;
    }
}
