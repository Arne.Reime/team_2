package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Enemy extends Entity implements IGameObject{
    private final int distance;
    private Image r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12;
    private Image t1, t2, t3;
    private double sum = 0;
    private int runCounter = 0;
    private int invincibleTimer = 50;

    public int takehitCounter = 0;

    public boolean canMoveDown = true;
    public boolean invincible = false;
    public boolean takehit = false;

    public int health = 3;

    public Enemy(double X, double Y, int distance) {
        GetEnemyImage();
        this.x = X;
        this.y = Y;
        this.w = 55;
        this.h = 80;
        this.drawW = 90;
        this.distance = distance;
        this.speed = 2.4;
    }

    /**
     * Ny konstruktør, bruk KUN til testing!
     * @param X kordinatet
     * @param Y kordinatet
     */
    public Enemy(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 55;
        this.h = 80;
        this.drawW = 90;
        this.distance = 50;
        this.speed = 2.4;
    }

    @Override
    public int getDrawWidth() {
        return this.drawW;
    }

    public void walk() {
        if(direction == 1) {
            this.x += this.speed;
            sum += this.speed;
        }
        if (direction == -1) {
            this.x -= this.speed;
            sum -= this.speed;
        }

        if (sum >= distance) direction = -1;
        else if (sum < 0)  direction = 1;


        if (canMoveDown) this.y += gravity;
    }

    public void GetEnemyImage() {
        this.r1 = new Image("file:src/Images/Enemy/enemyWalk1.png");
        this.r2 = new Image("file:src/Images/Enemy/enemyWalk2.png");
        this.r3 = new Image("file:src/Images/Enemy/enemyWalk3.png");
        this.r4 = new Image("file:src/Images/Enemy/enemyWalk4.png");
        this.r5 = new Image("file:src/Images/Enemy/enemyWalk5.png");
        this.r6 = new Image("file:src/Images/Enemy/enemyWalk6.png");
        this.r7 = new Image("file:src/Images/Enemy/enemyWalk7.png");
        this.r8 = new Image("file:src/Images/Enemy/enemyWalk8.png");
        this.r9 = new Image("file:src/Images/Enemy/enemyWalk9.png");
        this.r10 = new Image("file:src/Images/Enemy/enemyWalk10.png");
        this.r11 = new Image("file:src/Images/Enemy/enemyWalk11.png");
        this.r12 = new Image("file:src/Images/Enemy/enemyWalk12.png");

        this.t1 = new Image("file:src/Images/Enemy/enemyTakehit1.png");
        this.t2 = new Image("file:src/Images/Enemy/enemyTakehit2.png");
        this.t3 = new Image("file:src/Images/Enemy/enemyTakehit3.png");
    }

    @Override
    public Image getSprite() {
        if (runCounter > 55) {
            runCounter = 0;
        }

        //draw moving
        if (!takehit) {
            switch (runCounter) {
                case 0 -> this.image = r1;
                case 5 -> this.image = r2;
                case 10 -> this.image = r3;
                case 15 -> this.image = r4;
                case 20 -> this.image = r5;
                case 25 -> this.image = r6;
                case 30 -> this.image = r7;
                case 35 -> this.image = r8;
                case 40 -> this.image = r9;
                case 45 -> this.image = r10;
                case 50 -> this.image = r11;
                case 55 -> this.image = r12;
            }
        }

        if (this.takehit) {
            switch (takehitCounter) {
                case 0 -> this.image = t1;
                case 10 -> this.image = t2;
                case 20 -> this.image = t3;
                case 30 -> this.takehit = false;
            }
            takehitCounter++;
        }

        runCounter++;

        return this.image;
    }

    public void enemyInvincible() {
        if (invincibleTimer == 0) {
            invincible = false;
            invincibleTimer = 50;
        }
        invincibleTimer--;
    }

}
