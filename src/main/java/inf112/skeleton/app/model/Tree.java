package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Tree extends Entity implements IGameObject{

    public Tree(int X, int Y, String type) {
        this.x = X;
        this.y = Y;
        this.w = 200;
        this.h = 230;
        this.type = type;
        switch (type) {
            case "tree" -> this.img = new Image("file:src/Images/Objects/tree.png");
            case "pine" -> this.img = new Image("file:src/Images/Objects/pine.png");
        }
    }
    
    /**
     *  Use ONLY for testing!
     * @param X Coordinate
     * @param Y Coordinate
     */
    public Tree(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 200;
        this.h = 230;
        this.type = "";
    }
}

