package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Platform extends Entity implements IGameObject{

    public Platform(int X, int Y, int W, int H, String type) {
        this.x = X;
        this.y = Y;
        this.w = W;
        this.h = H;
        this.direction = 1;
        this.type = type;
        if (type == "grass")
            this.img = new Image("file:src/Images/Objects/grassplatform.png");
        if (type == "box")
            this.img = new Image("file:src/Images/Objects/box.png");
    }
    
    /**
     * 	Use ONLY for testing!
     *  Test does not run if we create a new instance of Image in the constructor.
     * @param X Coordinate
     * @param Y Coordinate
     * @param W Width
     * @param H height
     */
    public Platform(int X, int Y, int W, int H) {
        this.x = X;
        this.y = Y;
        this.w = W;
        this.h = H;
        this.type = "";
    }

}
