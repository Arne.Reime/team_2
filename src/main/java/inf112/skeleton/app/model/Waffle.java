package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Waffle extends Entity implements IGameObject {

    public Waffle(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 50;
        this.h = 50;
        this.img = new Image("file:src/Images/Objects/waffle.png");
    }
    
    /**
     *  Use ONLY for testing!
     * @param X Coordinate
     * @param Y Coordinate
     * @param test is not used
     */
    public Waffle(int X, int Y, int test) {
        this.x = X;
        this.y = Y;
        this.w = 50;
        this.h = 50;
    }
}
