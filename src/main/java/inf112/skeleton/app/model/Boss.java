package inf112.skeleton.app.model;

import javafx.scene.image.Image;
import java.util.Random;

public class Boss extends Entity implements IGameObject {
	private Image i1, i2, i3, i4;

	private int distance;
	private double sum = 0;
	private int invincibleTimer = 50;
	private int frames = 0;
	public int health = 7;
    public int runCounter = 0;

	public boolean invincible = false;
    public boolean canMoveDown = true;
	public boolean jump = false;

	private int n;
	Random rand = new Random();

    public Boss(int X, int Y, int distance) {
    	getBossImage();
    	this.x = X;
        this.y = Y;
        this.w = 70;
        this.h = 100;
        this.drawW = 100;
        this.distance = distance;
		this.speed = 3.1;
    }

	//Brukt for testing
    public Boss(int X, int Y) {
	    this.x = X;
	    this.y = Y;
	    this.w = 60;
	    this.h = 80;
	    this.drawW = 90;
	    this.distance = 50;
    }

	@Override
	public int getDrawWidth() {
		return this.drawW;
	}

	public void jumpBoss() {
		if (jump) {
			if (frames < n) {
				frames++;
				this.y -= 6;
			} else {
				n = rand.nextInt(50);
				frames = 0;
				canMoveDown = true;
				jump = false;
			}
		}

		if (canMoveDown) this.y += gravity;
	}

	public void bossInvincible() {
		if (invincibleTimer == 0) {
			invincible = false;
			invincibleTimer = 100;
		}
		invincibleTimer--;
	}

	public void walk() {
		if(direction == 1) {
			this.x += this.speed;
			sum += this.speed;
		}
		if (direction == -1) {
			this.x -= this.speed;
			sum -= this.speed;
		}

		if (sum >= distance) direction = -1;
		else if (sum < 0)  direction = 1;
	}
    
	private void getBossImage() {
		this.i1 = new Image("file:src/Images/Boss/idle1.png");
		this.i2 = new Image("file:src/Images/Boss/idle2.png");
		this.i3 = new Image("file:src/Images/Boss/idle3.png");
		this.i4 = new Image("file:src/Images/Boss/idle4.png");
	}

	@Override
	public Image getSprite() {

		if (runCounter > 35) runCounter = 0;

		switch (runCounter) {
			case 0 -> this.image = i1;
			case 10 -> this.image = i2;
			case 20 -> this.image = i3;
			case 30 -> this.image = i4;
		}

		runCounter++;

		return this.image;
	}

}
