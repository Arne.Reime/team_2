package inf112.skeleton.app.model;

import inf112.skeleton.app.view.Game;
import javafx.scene.image.Image;

public class NPC extends Entity implements IGameObject{
    private Image i1, i2, i3, i4, i5, i6, i7, i8, i9, i10, i11, i12, i13, i14;
    private int runCounter = 0;

    public String NPCDialogue;
    public boolean canMoveDown = true;
    public boolean interactableOption = false;

    public NPC(int X, int Y, String type) {
        this.x = X;
        this.y = Y;
        this.w = 120;
        this.h = 100;
        this.type = type;
        this.drawW = 70;
        this.direction = -1;
        GetNPCImage();
    }

    public int getDrawWidth() {
        return this.drawW;
    }

    public void idle() {
        if (canMoveDown) {
            this.y += gravity;
        }
    }

    public void GetNPCImage() {
        if (this.type == "BlueWizard") {
            this.i1 = new Image("file:src/Images/NPC/idle1.png");
            this.i2 = new Image("file:src/Images/NPC/idle2.png");
            this.i3 = new Image("file:src/Images/NPC/idle3.png");
            this.i4 = new Image("file:src/Images/NPC/idle4.png");
            this.i5 = new Image("file:src/Images/NPC/idle5.png");
            this.i6 = new Image("file:src/Images/NPC/idle6.png");
        }
        if (this.type == "RedWizard") {
            this.i7 = new Image("file:src/Images/NPC/idle7.png");
            this.i8 = new Image("file:src/Images/NPC/idle8.png");
            this.i9 = new Image("file:src/Images/NPC/idle9.png");
            this.i10 = new Image("file:src/Images/NPC/idle10.png");
            this.i11 = new Image("file:src/Images/NPC/idle11.png");
            this.i12 = new Image("file:src/Images/NPC/idle12.png");
            this.i13 = new Image("file:src/Images/NPC/idle13.png");
            this.i14 = new Image("file:src/Images/NPC/idle14.png");
        }
    }

    public Image getSprite() {
        if (this.type == "BlueWizard") {
            if (runCounter > 50) {
                runCounter = 0;
            }

            //draw idle
            switch (runCounter) {
                case 0 -> this.image = i1;
                case 10 -> this.image = i2;
                case 20 -> this.image = i3;
                case 30 -> this.image = i4;
                case 40 -> this.image = i5;
                case 50 -> this.image = i6;
            }
            runCounter += 1;

            return this.image;
        }

        if (this.type == "RedWizard") {
            if (runCounter > 70) {
                runCounter = 0;
            }

            //draw idle
            switch (runCounter) {
                case 0 -> this.image = i7;
                case 10 -> this.image = i8;
                case 20 -> this.image = i9;
                case 30 -> this.image = i10;
                case 40 -> this.image = i11;
                case 50 -> this.image = i12;
                case 60 -> this.image = i13;
                case 70 -> this.image = i14;
            }
            runCounter += 1;

            return this.image;
        }

        return null;
    }

    public void changeDirection(Game game) {
        if (game.player.getDirection() == 1) direction = -1;
        else  direction = 1;
    }
}
