package inf112.skeleton.app.model;

import inf112.skeleton.app.view.Game;
import javafx.scene.image.Image;

public class Player extends Entity implements IGameObject {
    private final int drawH;
    private int invincibleTimer = 100;

    public int health = 3;
    public int waffleScore = 0;
    public int frames = 0;
    public int runCounter = 0;
    public int atkCounter = 0;
    public double attackRecharge = 5.0;
    public int takehitCounter = 0;
    public boolean dead = false;

    public double spawnX = 45;
    public double spawnY = 50;

    public double screenX = Game.height/2;
    //public double screenY = Game.width/1.3;

    public boolean move_left = false;
    public boolean move_right = false;

    public boolean attack = false;
    public boolean jump = false;
    public boolean onGround = false;
    public boolean invincible = false;
    public boolean interact = false;
    public boolean canMoveLeft, canMoveRight, canMoveDown, canMoveUp;
    public boolean entered;
    public boolean keyItem = false;
    public double placeholderX, placeholderY;
    public boolean takehit = false;

    private Image r1, r2, r3, r4, r5, r6, r7, r8;
    private Image i1, i2, i3, i4, i5, i6, i7, i8;
    private Image h1, h2, h3, h4, h5, h6;
    private Image j1, j2, j3;
    private Image f1, f2, f3;
    private Image a12, a13, a14, a15, a16, a17;

    public Player() {
        this.x = 45;
        this.y = 300;
        this.w = 50;
        this.h = 100;
        this.drawW = 450;
        this.drawH = 225;
        GetPlayerImage();

        this.gravity = 5;
        this.speed = 5;
        this.direction = 1;
    }

    /**
     * Denne kontruktøren brukes bare for testing!
     */
    public Player(int test){
        this.x = 45;
        this.y = 300;
        this.w = 50;
        this.h = 100;
        this.drawW = 450;
        this.drawH = 225;
        this.speed = 5;
    }

    @Override
    public int getDrawHeight() {
        return this.drawH;
    }

    @Override
    public int getDrawWidth() {
        return this.drawW;
    }
    
    public void GetPlayer2Image() {
        //idle
        this.i1 = new Image("file:src/Images/NewPlayer2/01_idle/idle_1.png");
        this.i2 = new Image("file:src/Images/NewPlayer2/01_idle/idle_2.png");
        this.i3 = new Image("file:src/Images/NewPlayer2/01_idle/idle_3.png");
        this.i4 = new Image("file:src/Images/NewPlayer2/01_idle/idle_4.png");
        this.i5 = new Image("file:src/Images/NewPlayer2/01_idle/idle_5.png");
        this.i6 = new Image("file:src/Images/NewPlayer2/01_idle/idle_6.png");
        this.i7 = new Image("file:src/Images/NewPlayer2/01_idle/idle_7.png");
        this.i8 = new Image("file:src/Images/NewPlayer2/01_idle/idle_8.png");

        //run
        this.r1 = new Image("file:src/Images/NewPlayer2/02_run/run_1.png");
        this.r2 = new Image("file:src/Images/NewPlayer2/02_run/run_2.png");
        this.r3 = new Image("file:src/Images/NewPlayer2/02_run/run_3.png");
        this.r4 = new Image("file:src/Images/NewPlayer2/02_run/run_4.png");
        this.r5 = new Image("file:src/Images/NewPlayer2/02_run/run_5.png");
        this.r6 = new Image("file:src/Images/NewPlayer2/02_run/run_6.png");
        this.r7 = new Image("file:src/Images/NewPlayer2/02_run/run_7.png");
        this.r8 = new Image("file:src/Images/NewPlayer2/02_run/run_8.png");

        //jump
        this.j1 = new Image("file:src/Images/NewPlayer2/03_jump_up/jump_up_1.png");
        this.j2 = new Image("file:src/Images/NewPlayer2/03_jump_up/jump_up_2.png");
        this.j3 = new Image("file:src/Images/NewPlayer2/03_jump_up/jump_up_3.png");

        //fall
        this.f1 = new Image("file:src//Images/NewPlayer2/03_jump_down/jump_down_1.png");
        this.f2 = new Image("file:src//Images/NewPlayer2/03_jump_down/jump_down_2.png");
        this.f3 = new Image("file:src//Images/NewPlayer2/03_jump_down/jump_down_3.png");

        //attack
        this.a12 = new Image("file:src/Images/NewPlayer2/08_sp_atk/sp_atk_12.png");
        this.a13 = new Image("file:src/Images/NewPlayer2/08_sp_atk/sp_atk_13.png");
        this.a14 = new Image("file:src/Images/NewPlayer2/08_sp_atk/sp_atk_14.png");
        this.a15 = new Image("file:src/Images/NewPlayer2/08_sp_atk/sp_atk_15.png");
        this.a16 = new Image("file:src/Images/NewPlayer2/08_sp_atk/sp_atk_16.png");
        this.a17 = new Image("file:src/Images/NewPlayer2/08_sp_atk/sp_atk_17.png");
        
      //take hit
        this.h1 = new Image("file:src//Images/NewPlayer2/10_take_hit/take_hit_1.png");
        this.h2 = new Image("file:src//Images/NewPlayer2/10_take_hit/take_hit_2.png");
        this.h3 = new Image("file:src//Images/NewPlayer2/10_take_hit/take_hit_3.png");
        this.h4 = new Image("file:src//Images/NewPlayer2/10_take_hit/take_hit_4.png");
        this.h5 = new Image("file:src//Images/NewPlayer2/10_take_hit/take_hit_5.png");
        this.h6 = new Image("file:src//Images/NewPlayer2/10_take_hit/take_hit_6.png");
    }
    
    public void GetPlayerImage() {

        //idle
        this.i1 = new Image("file:src/Images/NewPlayer/01_idle/idle_1.png");
        this.i2 = new Image("file:src/Images/NewPlayer/01_idle/idle_2.png");
        this.i3 = new Image("file:src/Images/NewPlayer/01_idle/idle_3.png");
        this.i4 = new Image("file:src/Images/NewPlayer/01_idle/idle_4.png");
        this.i5 = new Image("file:src/Images/NewPlayer/01_idle/idle_5.png");
        this.i6 = new Image("file:src/Images/NewPlayer/01_idle/idle_6.png");
        this.i7 = new Image("file:src/Images/NewPlayer/01_idle/idle_7.png");
        this.i8 = new Image("file:src/Images/NewPlayer/01_idle/idle_8.png");

        //run
        this.r1 = new Image("file:src/Images/NewPlayer/02_run/run_1.png");
        this.r2 = new Image("file:src/Images/NewPlayer/02_run/run_2.png");
        this.r3 = new Image("file:src/Images/NewPlayer/02_run/run_3.png");
        this.r4 = new Image("file:src/Images/NewPlayer/02_run/run_4.png");
        this.r5 = new Image("file:src/Images/NewPlayer/02_run/run_5.png");
        this.r6 = new Image("file:src/Images/NewPlayer/02_run/run_6.png");
        this.r7 = new Image("file:src/Images/NewPlayer/02_run/run_7.png");
        this.r8 = new Image("file:src/Images/NewPlayer/02_run/run_8.png");

        //jump
        this.j1 = new Image("file:src/Images/NewPlayer/03_jump_up/jump_up_1.png");
        this.j2 = new Image("file:src/Images/NewPlayer/03_jump_up/jump_up_2.png");
        this.j3 = new Image("file:src/Images/NewPlayer/03_jump_up/jump_up_3.png");

        //fall
        this.f1 = new Image("file:src//Images/NewPlayer/03_jump_down/jump_down_1.png");
        this.f2 = new Image("file:src//Images/NewPlayer/03_jump_down/jump_down_2.png");
        this.f3 = new Image("file:src//Images/NewPlayer/03_jump_down/jump_down_3.png");

        //attack
        this.a12 = new Image("file:src/Images/NewPlayer/08_sp_atk/sp_atk_12.png");
        this.a13 = new Image("file:src/Images/NewPlayer/08_sp_atk/sp_atk_13.png");
        this.a14 = new Image("file:src/Images/NewPlayer/08_sp_atk/sp_atk_14.png");
        this.a15 = new Image("file:src/Images/NewPlayer/08_sp_atk/sp_atk_15.png");
        this.a16 = new Image("file:src/Images/NewPlayer/08_sp_atk/sp_atk_16.png");
        this.a17 = new Image("file:src/Images/NewPlayer/08_sp_atk/sp_atk_17.png");

        //take hit
        this.h1 = new Image("file:src//Images/NewPlayer/10_take_hit/take_hit_1.png");
        this.h2 = new Image("file:src//Images/NewPlayer/10_take_hit/take_hit_2.png");
        this.h3 = new Image("file:src//Images/NewPlayer/10_take_hit/take_hit_3.png");
        this.h4 = new Image("file:src//Images/NewPlayer/10_take_hit/take_hit_4.png");
        this.h5 = new Image("file:src//Images/NewPlayer/10_take_hit/take_hit_5.png");
        this.h6 = new Image("file:src//Images/NewPlayer/10_take_hit/take_hit_6.png");
    }

    public void Move() {
    	if(!this.dead) {
	        if (move_left && canMoveLeft) {
	            direction = -1;
	            this.x -= this.speed;
	        }
	
	        if (move_right && canMoveRight){
	            direction = 1;
	            this.x += this.speed;
	        }
	
	        if (jump && canMoveUp) {
	            onGround = false;
	            if(frames < 25) {
	                frames++;
	                this.y -= 6;
	            }
	            else {
	                frames = 0;
	                jump = false;
	                runCounter = 0;
	            }
	        }
	
	        if(!jump && canMoveDown) {
	            this.y += gravity;
	            onGround = false;
	        }
    	}
    }

    @Override
    public Image getSprite() {

        if (runCounter > 35) runCounter = 0;

        //draw moving left
        if (move_left && onGround && !attack && !takehit) {
            switch (runCounter) {
                case 0 -> this.image = r1;
                case 5 -> this.image = r2;
                case 10 -> this.image = r3;
                case 15 -> this.image = r4;
                case 20 -> this.image = r5;
                case 25 -> this.image = r6;
                case 30 -> this.image = r7;
                case 35 -> this.image = r8;
            }
        }

        //draw moving right
        if (move_right && onGround && !attack && !takehit) {
            switch (runCounter) {
                case 0 -> this.image = r1;
                case 5 -> this.image = r2;
                case 10 -> this.image = r3;
                case 15 -> this.image = r4;
                case 20 -> this.image = r5;
                case 25 -> this.image = r6;
                case 30 -> this.image = r7;
                case 35 -> this.image = r8;
            }
        }

        //draw not moving
        if (((!move_left && !move_right) || (move_left && move_right)) && onGround && !attack && !takehit) {
            switch (runCounter) {
                case 0 -> this.image = i1;
                case 5 -> this.image = i2;
                case 10 -> this.image = i3;
                case 15 -> this.image = i4;
                case 20 -> this.image = i5;
                case 25 -> this.image = i6;
                case 30 -> this.image = i7;
                case 35 -> this.image = i8;
            }
        }

        //draw jumping
        if (jump) {
            switch (runCounter) {
                case 0 -> this.image = j1;
                case 15 -> this.image = j2;
                case 30 -> this.image = j3;
            }
        }

        //draw falling
        if (!jump && !onGround) {
            switch (runCounter) {
                case 0 -> this.image = f1;
                case 15 -> this.image = f2;
                case 30 -> this.image = f3;
            }
        }

        //draw attack
        if (attack) {
            switch (atkCounter) {
                case 0 -> this.image = a12;
                case 4 -> this.image = a13;
                case 8 -> this.image = a14;
                case 12 -> this.image = a15;
                case 16 -> this.image = a16;
                case 18 -> this.image = a17;
            }
            atkCounter++;
        }

        //take hit
        if (takehit && !attack) {
            switch (takehitCounter) {
                case 0 -> this.image = h1;
                case 6 -> this.image = h2;
                case 12 -> this.image = h3;
                case 18 -> this.image = h4;
                case 24 -> this.image = h5;
                case 30 -> this.image = h6;
            }
            takehitCounter++;
        }

        runCounter++;

        return this.image;
    }

    public void Dead() {
        this.x = this.spawnX;
        this.y = this.spawnY;
        this.health --;
    }
    
    public void multiplayerDead(double x, double y) {
    	this.setCoordinates(x, y);
    	this.health --;
    	}

    public void setCoordinates(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public void previousCoordinates(double x, double y) {
        this.placeholderX = x;
        this.placeholderY = y;
    }

    public void playerTakehit() {
        if (takehitCounter >= 35) {
            takehit = false;
            takehitCounter = 0;
        }
    }

    public void playerAttack() {
        if (attack) {
            if (atkCounter > 18) attack = false;
        }

        if (attackRecharge < 50) {
            attackRecharge ++;
        }
    }

    public AttackArea attackHitbox(Player player) {
        return new AttackArea(player);
    }

    public void playerInvincible() {
        if (invincibleTimer == 0) {
            invincible = false;
            invincibleTimer = 100;
        }
        invincibleTimer--;
    }
    
}
