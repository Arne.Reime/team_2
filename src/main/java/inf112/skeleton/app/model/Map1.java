package inf112.skeleton.app.model;

import inf112.skeleton.app.view.Game;

public class Map1 {

    public static void map1Setup(Game game) {

        //SECTION 1

        game.ListTrees.add(new Tree(296, 315, "tree"));
        game.ListTrees.add(new Tree(900, 385, "tree"));

        game.ListPlatforms.add(new Platform(200, 480, 100, 100, "grass"));
        game.ListPlatforms.add(new Platform(296, 480, 100, 100, "grass"));

        int idx1 = 64;
        for (int i = 0; i < 12; i++) {
            game.ListPlatforms.add(new Platform(idx1, 550, 100, 100, "grass"));
            idx1 += 96;
        }

        game.ListEnemy.add(new Enemy(700, 250,  200));

        game.ListWaffle.add(new Waffle(300, 350));
        game.ListWaffle.add(new Waffle(1000, 400));

        //BREAK

        game.ListPlatforms.add(new Platform(1300, 390, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(1360, 390, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(1420, 390, 60, 60, "box"));

        //SECTION 2
        int idx2 = 1799;
        for (int i = 0; i < 5; i++) {
            game.ListPlatforms.add(new Platform(idx2, 440, 100, 100, "grass"));
            idx2 += 96;
        }

        game.ListNPC.add(new NPC(2000, 100, "BlueWizard"));

        int idx3 = 1696;
        for (int i = 0; i < 7; i++) {
            game.ListPlatforms.add(new Platform(idx3, 480, 100, 100, "grass"));
            idx3 += 96;
        }

        int idx4 = 1600;
        for (int i = 0; i < 8; i++) {
            game.ListPlatforms.add(new Platform(idx4, 550, 100, 100, "grass"));
            idx4 += 96;
        }

        game.ListTrees.add(new Tree(1880, 275, "tree"));
        game.ListWaffle.add(new Waffle(2300, 400));
        game.ListPlatforms.add(new Platform(2200, 360, 60, 60, "box"));

        //BREAK
        game.ListPlatforms.add(new Platform(2500, 360, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(2800, 360, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(3100, 360, 60, 60, "box"));

        //SECTION 3
        int idx5 = 3400;
        for (int i = 0; i < 10; i++) {
            game.ListPlatforms.add(new Platform(idx5, 550, 100, 100, "grass"));
            idx5 += 96;
        }
        game.ListCheckpoint.add(new Checkpoint(3600, 470));

        game.ListTrees.add(new Tree(3500, 385, "pine"));

        game.ListEnemy.add(new Enemy(3700, 250,  400));
        game.ListEnemy.add(new Enemy(3800, 250,  300));

        game.ListTrees.add(new Tree(4000, 385, "pine"));
        game.ListTrees.add(new Tree(4100, 385, "pine"));
        game.ListTrees.add(new Tree(4200, 385, "pine"));

        //BREAK
        game.ListPlatforms.add(new Platform(4400, 470, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(4410, 325, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(4720, 320, 60, 60, "box"));

        int idx6 = 5000;
        for (int i = 0; i < 10; i++) {
            game.ListPlatforms.add(new Platform(idx6, 380, 60, 60, "box"));
            idx6 += 60;
        }
        game.ListEnemy.add(new Enemy(5100, 200,  350));
        game.ListWaffle.add(new Waffle(5300, 120));
        game.ListWaffle.add(new Waffle(5500, 120));

        int idx7 = 5700;
        for (int i = 0; i < 17; i++) {
            game.ListPlatforms.add(new Platform(idx7, 250, 60, 60, "box"));
            idx7 += 60;
        }

        int idx8 = 5700;
        for (int i = 0; i < 17; i++) {
            game.ListPlatforms.add(new Platform(idx8, 520, 60, 60, "box"));
            idx8 += 60;
        }
        game.ListPlatforms.add(new Platform(6660, 460, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(6660, 400, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(6660, 340, 60, 60, "box"));

        game.ListEnemy.add(new Enemy(5800, 400,  200));
        game.ListEnemy.add(new Enemy(6300, 400,  250));
        game.ListEnemy.add(new Enemy(6100, 400,  300));
        game.ListEnemy.add(new Enemy(6000, 100,  400));

        game.ListWaffle.add(new Waffle(6600, 400));
        game.ListWaffle.add(new Waffle(6600, 350));

        //SECTION 4
        int idx11 = 464;
        for (int i = 0; i < 3; i++) {
            game.ListPlatforms.add(new Platform(7864, idx11, 100, 100, "grass"));
            idx11 -= 96;
        }
        int idx9 = 7000;
        for (int i = 0; i < 10; i++) {
            game.ListPlatforms.add(new Platform(idx9, 560, 100, 100, "grass"));
            idx9 += 96;
        }
        game.ListTrees.add(new Tree(7050, 395, "pine"));

        game.ListPlatforms.add(new Platform(7250, 480, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(7250, 420, 60, 60, "box"));
        game.ListPlatforms.add(new Platform(7130, 310, 60, 60, "box"));

        int idx10 = 7400;
        for (int i = 0; i < 10; i++) {
            game.ListPlatforms.add(new Platform(idx10, 230, 100, 100, "grass"));
            idx10 += 96;
        }
        game.ListEnemy.add(new Enemy(7500, 300, 200));
        game.ListWaffle.add(new Waffle(7750, 330));

        game.ListEnemy.add(new Enemy(7650, 100, 400));
        game.ListWaffle.add(new Waffle(7900, 100));

        //SECTION 5
        int idx12 = 8600;
        for (int i = 0; i < 20; i++) {
            game.ListPlatforms.add(new Platform(idx12, 560, 100, 100, "grass"));
            idx12 += 96;
        }

        game.ListWaffle.add(new Waffle(9050, 430));
        game.ListCheckpoint.add(new Checkpoint(8800, 480));
        game.ListTrees.add(new Tree(8700, 395, "pine"));
        game.ListTrees.add(new Tree(8900, 395, "pine"));
        game.ListTrees.add(new Tree(9200, 395, "pine"));
        game.ListTrees.add(new Tree(9300, 395, "pine"));

        game.ListHouse.add(new House(9740, 365, "2"));
        game.ListHouse.add(new House(9450, 310, "1"));

        game.ListEnemy.add(new Enemy(9300, 100, 350));
        game.ListEnemy.add(new Enemy(9600, 100, 470));
        game.ListEnemy.add(new Enemy(9850, 100, 200));

        game.ListNPC.add(new NPC(10200, 200, "RedWizard"));

        game.ListPortal.add(new Portal(10350, 450));

        //END
       int idx15 = 10524;
       for (int i = 0; i < 10; i++) {
           game.ListPlatforms.add(new Platform(idx15, 560, 100, 100, "grass"));
           idx15 += 96;
       }
        game.ListTrees.add(new Tree(10520, 395, "pine"));
        game.ListTrees.add(new Tree(10590, 395, "pine"));
        game.ListTrees.add(new Tree(10680, 395, "pine"));
        game.ListTrees.add(new Tree(10790, 395, "pine"));
        game.ListTrees.add(new Tree(10860, 395, "pine"));
        game.ListTrees.add(new Tree(10940, 395, "pine"));
        game.ListTrees.add(new Tree(11000, 395, "pine"));
        game.ListTrees.add(new Tree(11090, 395, "pine"));
        game.ListTrees.add(new Tree(11150, 395, "pine"));
        game.ListTrees.add(new Tree(11210, 395, "pine"));


        //HOUSE ROOM 1
        int idx13 = -2200;
        for (int i = 0; i < 15; i++) {
            game.ListPlatforms.add(new Platform(idx13, 520, 60, 60, "box"));
            idx13 += 60;
        }
        int idx14 = -2200;
        for (int i = 0; i < 15; i++) {
            game.ListPlatforms.add(new Platform(idx14, 100, 60, 60, "box"));
            idx14 += 60;
        }
        int idx16 = 460;
        for (int i = 0; i < 6; i++) {
            game.ListPlatforms.add(new Platform(-2200, idx16, 60, 60, "box"));
            idx16 -= 60;
        }
        int idx17 = 460;
        for (int i = 0; i < 6; i++) {
            game.ListPlatforms.add(new Platform(-1360, idx17, 60, 60, "box"));
            idx17 -= 60;
        }
        game.ListFridge.add(new Fridge(-1600, 435));
    }
}
