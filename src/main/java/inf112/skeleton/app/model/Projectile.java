package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Projectile extends Entity implements IGameObject{

    private Image p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;

    public int health = 100;
    private final int numDir;
    private int runCounter = 0;

    public Projectile(Boss boss) {
        numDir = boss.getDirection();
        this.x = 20 * boss.getDirection() + boss.getX();
        this.y = boss.getY();
        this.w = 30;
        this.h = 30;
        this.drawW = 100;
        this.speed = 7;
        getProjectileImage();
    }

    @Override
    public int getDrawWidth() {
        return this.drawW;
    }

    @Override
    public int getDirection() {
        return -this.numDir;
    }

    public void move() {
        health--;
        if (numDir == 1) {
            this.x += speed;
        }
        else {
            this.x -= speed;
        }
    }

    public void getProjectileImage() {
        this.p1 = new Image("file:src/Images/Projectile/Fireball1.png");
        this.p2 = new Image("file:src/Images/Projectile/Fireball2.png");
        this.p3 = new Image("file:src/Images/Projectile/Fireball3.png");
        this.p4 = new Image("file:src/Images/Projectile/Fireball4.png");
        this.p5 = new Image("file:src/Images/Projectile/Fireball5.png");
        this.p6 = new Image("file:src/Images/Projectile/Fireball6.png");
        this.p7 = new Image("file:src/Images/Projectile/Fireball7.png");
        this.p8 = new Image("file:src/Images/Projectile/Fireball8.png");
        this.p9 = new Image("file:src/Images/Projectile/Fireball9.png");
        this.p10 = new Image("file:src/Images/Projectile/Fireball10.png");
    }

    public Image getSprite() {

        if (runCounter > 50) runCounter = 0;

        switch (runCounter) {
            case 0 -> this.image = p1;
            case 5 -> this.image = p2;
            case 10 -> this.image = p3;
            case 15 -> this.image = p4;
            case 20 -> this.image = p5;
            case 25 -> this.image = p6;
            case 30 -> this.image = p7;
            case 35 -> this.image = p8;
            case 40 -> this.image = p9;
            case 45 -> this.image = p10;
        }
        runCounter++;

        return this.image;
    }

}
