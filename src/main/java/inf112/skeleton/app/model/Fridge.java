package inf112.skeleton.app.model;

import javafx.scene.image.Image;

public class Fridge extends Entity implements IGameObject{
    public boolean drawText;

    public Fridge(int X, int Y) {
        this.x = X;
        this.y = Y;
        this.w = 100;
        this.h = 110;
        this.drawW = 70;
        this.img = new Image("file:src/Images/Objects/fridge.png");
    }

    @Override
    public int getDrawWidth() {
        return this.drawW;
    }
}


