package inf112.skeleton.app.model;

public class AttackArea extends Entity implements IGameObject{

    public AttackArea(Player player) {
        this.x = 120 * player.getDirection()+ player.getX();
        this.y = player.getY();
        this.w = 80;
        this.h = 60;
        }
}
