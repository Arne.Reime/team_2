package inf112.skeleton.app.model;

import inf112.skeleton.app.view.Game;

public class Map2 {

    public static void map2Setup(Game game) {

        game.ListBoss.add(new Boss(900, 400, 600));

        game.ListPortal.add(new Portal(0, 300));

        int idx0 = 40;
        for (int i = 0; i < 10; i++) {
            game.ListPlatforms.add(new Platform(-120, idx0, 60, 60, "box"));
            idx0 += 60;
        }

        game.ListTrees.add(new Tree(20, 495, "pine"));
        game.ListTrees.add(new Tree(200, 495, "pine"));
        game.ListTrees.add(new Tree(320, 495, "pine"));
        game.ListTrees.add(new Tree(450, 495, "pine"));
        game.ListTrees.add(new Tree(520, 495, "pine"));
        game.ListTrees.add(new Tree(610, 495, "pine"));

        game.ListPlatforms.add(new Platform(650, 490, 50, 30, "box"));

        game.ListCheckpoint.add(new Checkpoint(130, 580));

        int idx1 = -100;
        for (int i = 0; i < 20; i++) {
            game.ListPlatforms.add(new Platform(idx1, 660, 100, 100, "grass"));
            idx1 += 96;
        }
    }
}
