module inf112.skeleton.app {
	exports inf112.skeleton.app;
	exports inf112.skeleton.app.view;
	exports inf112.skeleton.app.Controller;
	exports inf112.skeleton.app.model;

	requires transitive javafx.base;
	requires transitive javafx.graphics;
	requires transitive javafx.controls;
    requires java.desktop;


}
