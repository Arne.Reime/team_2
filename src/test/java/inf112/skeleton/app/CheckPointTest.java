package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import inf112.skeleton.app.model.Checkpoint;

public class CheckPointTest{
	Checkpoint checkpoint;
	
	@BeforeEach
	void setup() {
		this.checkpoint = new Checkpoint(0, 0, 1);
	}
	
    @Test
	void TestgetHeight() {
	    assertEquals(checkpoint.getHeight(), 60);
	}
	@Test
	void TestgetWidth() {
		assertEquals(checkpoint.getWidth(), 40);
	}
	@Test
	void TestgetX() {
		assertEquals(checkpoint.getX(), 0);
	}
	@Test
	void TestgetY() {
	    assertEquals(checkpoint.getY(), 0);
	}
	@Test
	void TestgetSpeed() {
		assertEquals(checkpoint.getSpeed(), 0);
	}
	@Test
	void TestgetGravity() {
	    assertEquals(checkpoint.getGravity(), 5);
	}
}
