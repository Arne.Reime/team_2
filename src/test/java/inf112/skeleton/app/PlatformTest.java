package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;

import inf112.skeleton.app.model.Platform;
import org.junit.jupiter.api.*;

public class PlatformTest {
	Platform platform;
	@BeforeEach
	void setup() {
		this.platform = new Platform(0, 0, 100, 100);
	}
	
    @Test
	void TestgetHeight() {
	    assertEquals(platform.getHeight(), 100);
	}
	@Test
	void TestgetWidth() {
		assertEquals(platform.getWidth(), 100);
	}
	@Test
	void TestgetX() {
		assertEquals(platform.getX(), 0);
	}
	@Test
	void getY() {
	    assertEquals(platform.getY(), 0);
	}
	@Test
	void getSpeed() {
		assertEquals(platform.getSpeed(), 0);
	}
	@Test
	void getGravity() {
	    assertEquals(platform.getGravity(), 5);
	}
}
