package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import inf112.skeleton.app.model.Waffle;

public class WaffleTest{
	Waffle waffle;
	@BeforeEach
	void setup() {
		this.waffle = new Waffle(0, 0, 1);
	}
	
    @Test
	void TestgetHeight() {
	    assertEquals(waffle.getHeight(), 50);
	}
	@Test
	void TestgetWidth() {
		assertEquals(waffle.getWidth(), 50);
	}
	@Test
	void TestgetX() {
		assertEquals(waffle.getX(), 0);
	}
	@Test
	void getY() {
	    assertEquals(waffle.getY(), 0);
	}
	@Test
	void getSpeed() {
		assertEquals(waffle.getSpeed(), 0);
	}
	@Test
	void getGravity() {
	    assertEquals(waffle.getGravity(), 5);
	}
}
