package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import inf112.skeleton.app.model.Player;

public class PlayerTest{
	Player player;
	@BeforeEach
	void setup() {
		this.player = new Player(1);
	}
	
	@Test
	void MoveRightTest(){
		//Player player = new Player();
		player.canMoveDown = false;
		player.canMoveUp = false;
		player.canMoveLeft = false;
		player.canMoveRight = true;
		player.move_right = true;
		double x2 = player.getX();
		player.Move();
		assertEquals(player.getX(), player.getSpeed()+x2);
	}
	
	@Test
	void MoveLeftTest(){
		//Player player = new Player(1);
		player.canMoveDown = false;
		player.canMoveUp = false;
		player.canMoveLeft = true;
		player.canMoveRight = false;
		player.move_left = true;
		double x2 = player.getX();
		player.Move();
		assertEquals(player.getX(), x2-player.getSpeed());
	}

	@Test
	void MoveUpTest(){
		//Player player = new Player(1);
		player.canMoveDown = false;
		player.canMoveUp = true;
		player.canMoveLeft = false;
		player.canMoveRight = false;
		player.jump = true;
		double y2 = player.getY();
		player.Move();
		assertEquals(player.getY(), y2-6);
	}
	
	@Test
	void MoveDownTest(){
		//Player player = new Player();
		player.canMoveDown = true;
		player.canMoveUp = false;
		player.canMoveLeft = false;
		player.canMoveRight = false;
		double y2 = player.getY();
		player.Move();
		assertEquals(player.getY(), y2+player.getGravity());
	}
	
	@Test
	void DeadTest(){
		//Player player = new Player();
		int health = player.health;
		player.Dead();
		assertEquals(player.health, health-1);
	}

    @Test
	void TestgetHeight() {
	    assertEquals(player.getHeight(), 100);
	}
    @Test
	void TestgetWidth() {
		assertEquals(player.getWidth(), 50);
	}
    @Test
	void TestgetX() {
		assertEquals(player.getX(), 45);
	}
    @Test
	void getY() {
	    assertEquals(player.getY(), 300);
	}
    @Test
	void getSpeed() {
		assertEquals(player.getSpeed(), 5);
	}
    @Test
	void getGravity() {
	    assertEquals(player.getGravity(), 5);
	}
}
