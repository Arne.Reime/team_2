package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import inf112.skeleton.app.model.Enemy;
import inf112.skeleton.app.model.Player;


public class EnemyTest {
	Enemy enemy;
	
	@BeforeEach
	void setup() {
		this.enemy = new Enemy(0, 0);
	}
	
    @Test
	void TestgetHeight() {
	    assertEquals(enemy.getHeight(), 80);
	}
    @Test
	void TestgetWidth() {
		assertEquals(enemy.getWidth(), 55);
	}
    @Test
	void TestgetX() {
		assertEquals(enemy.getX(), 0);
	}
    @Test
	void TestgetY() {
	    assertEquals(enemy.getY(), 0);
	}
    @Test
	void TestgetSpeed() {
		assertEquals(enemy.getSpeed(), 2.4);
	}
    @Test
	void TestgetGravity() {
	    assertEquals(enemy.getGravity(), 5);
	}
    
    @Test
    void TestWalk() {
		double x = enemy.getX();
    	enemy.walk();
    	assertEquals(enemy.getX(), x + enemy.getSpeed());
    }
    
}
