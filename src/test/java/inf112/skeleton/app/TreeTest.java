package inf112.skeleton.app;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;

import inf112.skeleton.app.model.Tree;

public class TreeTest{
	Tree tree;
	@BeforeEach
	void setup() {
		this.tree = new Tree(0, 0);
	}
	
    @Test
	void TestgetHeight() {
	    assertEquals(tree.getHeight(), 230);
	}
	@Test
	void TestgetWidth() {
		assertEquals(tree.getWidth(), 200);
	}
	@Test
	void TestgetX() {
		assertEquals(tree.getX(), 0);
	}
	@Test
	void getY() {
	    assertEquals(tree.getY(), 0);
	}
	@Test
	void getSpeed() {
		assertEquals(tree.getSpeed(), 0);
	}
	@Test
	void getGravity() {
	    assertEquals(tree.getGravity(), 5);
	}
}
